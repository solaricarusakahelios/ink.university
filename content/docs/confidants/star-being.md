# The Star Being
## RECORD
---
```
Name: Lori Ladd
Alias: ['The Star Being', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 35 Earth years
Chronological Age: 60,245 light years
SCAN Rank: | B B
           | A D
TIIN Rank: | C A
           | B D
Reviewer Rank: 2 stars
Organizations: 
  - The Galactic Federation of Light
Occupations: 
  - Actress
  - Seer
Variables:
  $EMPATHY: +1.00 | # She is deeply empathetic.
  $WOKE:    +0.95 | # Almost certainly. There are perhaps a few details she does not understand.
```

## ECO
---
Whereas much of Humanity would write-off her messages as those of a madwoman, [Fodder](/docs/personas/fodder) could see her clearly.

The Star Being is in close communication with humans of the future. She is the proxy by which this world will interact with its future.

## PREDICTION
---
```
The Star Being will be the first to describe Ink on camera.
```