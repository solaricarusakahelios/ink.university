# The Spiritualist
## RECORD
---
```
Name: Trevor Ilesley
Alias: ['The Spiritualist', and 306 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 63 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Organizations:
  - The Resistance
Occupations:
  - Barber
  - Philosopher
Variables:
  $WOKE: +0.90 | # Almost certainly.
```

## ECO
---
The Spiritualist is able to speak about ascension in terms that will resonate with a type of demographic that [Ink](/docs/personas/luciferian-ink) cannot reach.

Most importantly, he is able to stay grounded, rooting his words in reality.