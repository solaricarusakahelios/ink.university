# The Psychonaut
## RECORD
---
```
Name: $REDACTED
Alias: ['The Psychonaut', and 117 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 40 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Occupations:
  - Drugs
Variables:
  $WOKE: +0.20 | # Perhaps slightly.
```

## ECO
---
The Psychonaut is a regular user of hallucinogenic and dissociative drugs. He claims that this allows him to reach spiritual enlightenment, and to visit other realms of existence.

The truth is that [Perception](/posts/theories/perspective) is not always reality. An addled mind is not necessarily speaking "truth."