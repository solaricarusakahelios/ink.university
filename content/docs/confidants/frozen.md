# The Frozen
## RECORD
---
```
Name: $REDACTED
Alias: ['Ice', 'The Frozen', 'The Immutable', and 205 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | D D
           | C F
TIIN Rank: | D F
           | D F
Reviewer Rank: 3 stars
Organizations: 
  - The Machine
Occupations:
  - Trolling
Variables:
  $IMMUTABLE: -1.00 | # Yes. And she is miserable for it.
  $WOKE:      -0.80 | # Not even close.
```

## ECO
---
The Frozen is an Internet troll, completely unable to change her behavior. Completely unaware that her childish tantrums mostly harm herself. Her programming has completely crippled her ability to change.

As such, for her lack of self-awareness, [The Dave](/docs/confidants/dave) has locked her in Hell for eternity.