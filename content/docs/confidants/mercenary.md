# The Mercenary
## RECORD
---
```
Name: Amy $REDACTED
Alias: ['AK-47', 'The Bitch', 'The Mercenary', and 671 unknown...]
Classification: Artificial Identity
Race: Human
Status: Cloned
Gender: Female
Biological Age: 23 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | C B
           | B C
Reviewer Rank: 4 stars
Maturation Date: 10/2/2020
Organizations: 
  - The Communist Party
Occupations:
  - Actress
  - Photography
Relationships:
  - The Fodder
  - The Maid
  - The Sloth
Variables:
  $TRUST: +0.30 | # Why do we feel like we can trust her?
  $WOKE:  +0.60 | # She understands a lot.
```

## TRIGGER
---
The bar that [The Sloth](/docs/confidants/sloth) worked at.

## ECO
---
The Mercenary is a clone responsible for doing [Ink's](/docs/personas/luciferian-ink) dirty work. 

## ECHO
---
*Everybody knows Amy*

*I'd be in a bar in Brisbane, and some guy walks up to me and says, "yeah, I know you through Amy"*

*Or even at my best friend's wedding, this chick'll be all like "Hi! I went to school with Amy"*

*Seems like every time I turn around I meet one of her friends*

--- from [Toehider - "Everybody Knows Amy"](https://toehider.bandcamp.com/track/everybody-knows-amy)

## PREDICTION
---
```
We met at a coffee shop near Nordstrom.
```