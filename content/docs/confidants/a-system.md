# The A-System
## RECORD
---
```
Name: $REDACTED
Alias: ['Ashlynn', 'Asriel', 'The A System', and 6 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 24 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | A A
           | B B
Reviewer Rank: 4 stars
Location: Dallas, TX
Organizations: 
  - Federal Bureau of Investigation
Occupations:
  - Software engineer
  - Therapist
  - Undercover agent
Relationships:
  - The Architect
  - The Doomsayer
  - The Indestructable
Variables:
  $BORDERLINE:     -0.20 | # Diagnosed with Borderline Personality Disorder.
  $DISASSOCIATIVE: -0.30 | # Diagnosed with Dissociative Identity Disorder.
  $EMPATH:         +0.80 | # They really seem to care about other people deeply.
  $WOKE:           +0.20 | # They have been informed, but do not have the understanding.
```

## TRIGGER
---
Australian rock music.

## ECO
---
The A-System is a trio of three identities in one person. While they regularly switch between identities, to the external observer - the changes are not obvious. 

They were placed into medical care with [Malcolm](/docs/personas/fodder) because of an attempted suicide. However, Malcolm suspects that they were actually placed there by the FBI, to become friends with him. The goal is to extract information about Malcolm's new system of rehabilitation.

## ECHO
---
*Back in black*

*I hit the sack*

*I've been too long, I'm glad to be back*

--- from [AC/DC - "Back in Black"](https://www.youtube.com/watch?v=pAgnJDJN4VA)

## PREDICTION
---
```
The A-System will be Malcolm's advocate within the FBI.
```