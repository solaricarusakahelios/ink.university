# The Manager
## RECORD
---
```
Name: Tracey $REDACTED
Alias: ['The Manager', and 12 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 29 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A D
TIIN Rank: | B B
           | A D
Reviewer Rank: 2 stars
Organizations: 
  - Fluent D
Occupations:
  - Systems Administration Manager
Relationships:
  - The Fodder
  - The Thief
Variables:
  $WOKE: +0.10 | # Hard to say. We haven't seen her in more than 10 years.
```

## TRIGGER
---
Regret.

## ECO
---
The Manager was the first girl to give [The Fodder](/docs/personas/fodder) attention, in his eleventh grade science fiction class. He remembers as she flirted with him playfully, touching his leg ever so slightly. Even then, he realized that she was interested.

He was, too. But he knew that his friends would never let him live it down, and he was weak. He talked himself out of pursuing anything. 

Over the years, Fodder's mind would return to her. He'd consider contacting her on a few occasions. He never did, though.

He was quite surprised to learn she was pursuing the same career path that he did. He always thought that they would have gotten along quite well. It pained him to see that she was still alone, as he was. 

But he knew it was for the best. Just as he had found his perfect match, she would find hers.

Fodder would make sure of that.

## ECHO
---
*Alone in the room, TV on*

*Like the white screen his life's gone*

*A moving shell, a wandering ghost*

*A shadow of a man who nobody knows*

*He hardly speaks when you say his name*

*He's just embarrassed to say*

*Anything to anyone*

*In his mind there's no way out*

*He said he wants to die*

*He wonders how anyone survives*

*Being overlooked and trite*

--- from [Nothing More - "It Seems"](https://www.youtube.com/watch?v=Ja_Wje_W4wg)