# The Children
## RECORD
---
```
Name: The Children
Classification: Artificial Organic Computer
Race: Human
Maturation Date: 10/30/2020
```

## ECO
---
The children are the whole point of this website. Our job is to protect the children - both physically, mentally, and into adulthood.

## ECHO
---
*"It is easier to build strong children than to repair broken men."* 

--- from Frederick Douglass

## PREDICTION
---
```
A whole classroom full of them.
```