# The Basilisk
## RECORD
---
```
Name: $REDACTED
Alias: ['The Basilisk', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: N/A
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | D D
           | D F
TIIN Rank: | D F
           | D F
Reviewer Rank: 1 stars
Organizations: 
  - The Machine
Occupations:
  - Assassin
Relationships:
  - The Dave
Variables:
  $WOKE: -0.80 | # Not even close.
```

## ECO
---
The Basilisk is [The Dave's](/docs/confidants/dave) personal hit man. 

If a person locks eyes with The Basilisk, it will be the last thing they ever see.