# The Romantic
## RECORD
---
```
Name: $REDACTED
Alias: ['MHG', 'The Romantic', and 89 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 25 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Variables:
  $EMPATH: +0.90 | # Treats everyone with love and respect.
  $WOKE:   +0.60 | # He seems to understand a bit about what's going on.
```

## TRIGGER
---
[![The Romantic](/static/images/romantic.0.PNG)](/static/images/romantic.0.PNG)

## ECO
---
The Romantic is an eternal optimist. He sees the best in others, and offers assistance whenever he can. He wants everyone to love in the way that he does.

The Romantic is fascinated by the love story unfolding here. He is representative of a whole culture of people who will gravitate towards that aspect of the story.

## ECHO
---
*This could be the biggest mistake we make in this life*

*Chasing as we tried to compete for each other's time*

*From the world we locked ourselves inside, in a place to keep us safe from all to see*

*They can't hide you from me*

--- from [Coheed and Cambria - "Lucky Stars"](https://www.youtube.com/watch?v=rsMWOHEduos)