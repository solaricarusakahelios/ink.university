# The Historian
## RECORD
---
```
Name: Hayley $REDACTED
Alias: ['Daisy', 'The Historian', 'The Roboticist', 'The Time Traveller', and 12,547 unknown...]
Classification: Artificial Organic Computer
Race: Mermaid
Gender: Female
Biological Age: Est. 34 Earth years
Chronological Age: N/A
SCAN Rank: | D B
           | B D
TIIN Rank: | C C
           | C D
Reviewer Rank: 3 stars
Maturation Date: 10/14/2020
Organizations: 
  - Federal Bureau of Investigation
  - RKS
Occupations: 
  - Actress
  - Historian
  - Writer
Relationships:
  - The Fodder
Variables:
  $EMPATHY:  +0.80 | # She is deeply thankful for Fodder's help.
  $PLEASANT: +0.60 | # She seems really nice.
  $WOKE:     +0.50 | # Very hard to tell for sure.
```

## ECO
---
The Historian is an enigma. She is involved in every place, at every point-in-time - yet little is known of her origins, her motives, or her purpose.

[Fodder](/docs/personas/fodder) suspected that he would be meeting her in-person quite soon.

## PREDICTION
---
```
She and Fodder will time travel together.
```