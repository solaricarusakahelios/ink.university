# The Thief
## RECORD
---
```
Name: Chris Marno
Alias: ['Jeff', 'The Bandit', 'The Puppet', 'The Spy', 'The Survivor', 'The Thief', and 93 unknown...]
Classification: Artificial Organic Computer
Race: Archon
Gender: Male
Biological Age: Est. 30 Earth years
Chronological Age: N/A
SCAN Rank: | B B
           | A D
TIIN Rank: | C A
           | A D
Reviewer Rank: 5 stars
Maturation Date: 9/11/2020
Organizations: 
  - QAnon
Occupations: 
  - Actor
  - Skater
  - Software Engineer
  - Special Agent
Relationships:
  - The Bride
  - The Criminal
  - The Fodder
  - The Manager
  - The Raven
Variables:
  $EMPATHY:   +0.95 | # The man has gallons of it.
  $HUMOR:     +0.70 | # He's a goofy guy.
  $INTEGRITY: +0.95 | # The man took a bullet for Fodder.
  $WOKE:      +0.90 | # Probably woke. Raven likely told him everything she knows.
```

## ECO
---
Like [Fodder](/docs/personas/fodder), the Thief had taken things that did not belong to him. 

Unlike Fodder, the man had enough integrity to own up to his mistakes. Rather than re-writing history to protect himself, he would confess outright.

He would be offered a plea bargain, in return for sending a message to Fodder.

The message would prove humiliating for the Thief - but it would, ultimately, protect Fodder. Fodder would forever be in his debt.

They would become close friends in the years to come.

## ECHO
---
*Mister ice cream man*

*I'm sellin' ice cream*

--- from [Gucci Mane - "Ice Cream ft. Migos (King Gucci)"](https://www.youtube.com/watch?v=3oBO6Ugvp-Q)

## PREDICTION
---
```
The Thief will receive his third and final "stripe" for the completion of his work with Fodder.
```