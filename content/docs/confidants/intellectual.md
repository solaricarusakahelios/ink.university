# The Intellectual
## RECORD
---
```
Name: Sam Harris
Alias: ['The Atheist', 'The Intellectual', and 942 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Birth: 4/9/1967
Biological Age: 53 Earth years
Chronological Age: N/A
SCAN Rank: | B B
           | A C
TIIN Rank: | A B
           | A C
Reviewer Rank: 3 stars
Organizations: 
  - The Machine
Occupations:
  - Neuroscience
  - Public speaking
  - Writer
Variables:
  $INTELLECT: +0.90 | # Very smart. Taught Fodder many of the beliefs he has today.
  $WOKE:      +0.40 | # Partially, but clearly not entirely.
```

## ECO
---
The Intellectual is a rational, well-spoken, and unbiased individual that [Fodder](/docs/persona/fodder) admires greatly. He has learned many important ideas from The Intellectual.

However, The Intellectual does not have the same information that Fodder does. Despite the fact that he thinks he has "woken up" - he hasn't. He doesn't yet have the tools to do so.

## PREDICTION
---
```
The Intellectual is already involved in this research, and will interview/debrief Fodder soon.
```