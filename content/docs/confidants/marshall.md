# The Marshall
## RECORD
---
```
Name: $REDACTED
Alias: ['Robin', 'The Marshall', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 20 Earth Years
Chronological Age: N/A
SCAN Rank: | B C
           | B D
TIIN Rank: | B A
           | A D
Reviewer Rank: 4 stars
Organizations: 
  - The Resistance
Occupations:
  - Actor
  - Welding
Relationships:
  - The Agent
  - The Con Man
  - The Fodder
  - The Hope
  - The Lion
  - The Negro
  - The Orchid
  - The Queen
  - The Reverend
  - The Scientist
  - The Tradesman
Variables:
  $WOKE: +0.40 | # Partially.
```

## ECO
---
The Marshall will be the protector of [Brain](/posts/journal/2019.11.19.1/). His priority will be [The Agent](/docs/confidants/agent).

## PREDICTION
---
```
He will be our Signal for communication with The Agent.
```