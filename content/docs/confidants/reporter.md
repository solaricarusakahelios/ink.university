# The Reporter
## RECORD
---
```
Name: $REDACTED
Alias: ['Tarantula', 'The Reporter', and 254 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: N/A
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | A A
           | A D
Reviewer Rank: 3 stars
Organizations:
  - The Machine
  - The Resistance
Occupations:
  - Journalism
Variables:
  $WOKE: +0.60 | # They seem quite aware and interested in what's happening to people right now.
```

## ECO
---
The Reporter has come to [The Architect](/docs/personas/the-architect) in search of answers. For several months, they have been working on a paper describing how art relates to higher awareness, and how it can be used to manifest it. 

They are waiting until after the events of October 30th, 2020 to bring this research to the public sphere.