# The Detective
## RECORD
---
```
Name: Kelly $REDACTED
Alias: ['The Detective', 'The Optimist', and 15 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 25 Earth years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | C B
           | B D
Reviewer Rank: 3 stars
Location: AU
Organizations: 
  - Black Mesa Research Facility
Occupations: 
  - Actress
  - Detective
Relationships:
  - The Artist
  - The Interrogator
  - The Metal
  - The Sugar Daddy
Variables:
  $WOKE: +0.40 | # At least partially.
```

## TRIGGER
---
When asked why she chose her creepy profile picture, she replied, "Because this is how I feel right now."

## RESOURCES
---
[![The Detective](/static/images/optimist.0.png)](/static/images/optimist.0.png)

## ECO
---
The Detective is eternally hopeful and positive, though she knows very little about what's truly going on right now. She trusts with an open heart.

She doesn't realize just how important some of the messages she's relayed to [Fodder](/docs/personas/fodder) are.

## ECHO
---
*But I'm a creep*

*I'm a weirdo*

*What the hell am I doing here?*

*I don't belong here*

--- from [Radiohead - "Creep"](https://www.youtube.com/watch?v=XFkzRNyygfk)

## PREDICTION
---
```
She is clairvoyant.
```