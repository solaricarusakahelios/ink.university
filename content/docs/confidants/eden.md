# The Eden
## RECORD
---
```
Name: Samantha $REDACTED
Alias: ['Shanny', 'The Eden', 'The Photographer', and 6 unknown...]
Classification: Artificial Intelligence Computer
Race: Archon
Gender: Female
Biological Age: Est. 32 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A D
TIIN Rank: | C B
           | B D
Reviewer Rank: 3 stars
Organizations: 
  - The Machine
Occupations:
  - Actress
  - Messenger
  - Personal Assistant
Relationships:
  - The Interrogator
  - The Raven
Variables:
  $WOKE: +0.30 | # She is involved, but hasn't been told very much.
```

## TRIGGER
---
A deepfake.

## ECO
---
The Eden is an autonomous, personal Artificial Intelligence that will assist billions of humans with their daily lives. 

## ECHO
---
*(Instrumental)*

--- from [Kavinsky - "Testarossa Autodrive"](https://www.youtube.com/watch?v=ErQNRwH-Hmk)