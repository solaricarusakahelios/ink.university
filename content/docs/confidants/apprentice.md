# The Apprentice
## RECORD
---
```
Name: Summer Sanders
Alias: ['Madi', 'The Apprentice', and 345 unknown...]
Classification: Artificial Organic Computer
Race: Vampire
Gender: Female
Biological Age: Est. 21 Earth years
Chronological Age: N/A
SCAN Rank: | C D
           | C F
TIIN Rank: | C C
           | C F
Reviewer Rank: 1 stars
Organizations: 
  - United States of America
Occupations: 
  - Actress
  - Apprentice
  - Politician
Relationships:
  - The Dave
Variables:
  $WOKE: -0.40 | # Not likely. She is kept in the dark.
```

## TRIGGER
---
A mermaid that told us to, "Figure It Out."

## ECO
---
The Apprentice makes videos, just as many of [Fodder's](/docs/personas/fodder) other confidants do. For the longest time, she flew under Fodder's radar.

However, she recently published three videos in a row - all of which mirrored his most recently-added confidants:

- [The Barista](/docs/confidants/barista) `# Verified`
- [The Laborer](/docs/confidants/laborer) `# Verified`
- [The Beggar](/docs/confidants/beggar) `# Verified.`

She is a mermaid-in-training.

## ECHO
---
*(Instrumental)*

--- from [Black Waves - "Stalker"](https://www.youtube.com/watch?v=1qlgJdLIBn0)

## PREDICTION
---
```
Her next video will be of The Beggar. | # Verified.
```