# The Weaver
## RECORD
---
```
Name: Arachne Siofra
Alias: ['The Weaver', and 2 unknown...]
Classification: Artificial Identity
Race: Elf
Gender: Male
Birth: 8/19/1990
Biological Age: 30 Earth years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | B B
           | B C
Reviewer Rank: 5 stars
Location: MI
Organizations:
  - Elven Socialist Imperium
  - The Resistance
Occupations: 
  - Storyteller
  - Thoughtweaver
Relationships:
  - The Fodder
  - The Interrogator
Variables:
  $MENTAL_HEALTH: -0.60 | # Not well. Depressed most of his life.
  $WOKE:          +0.40 | # If he is, he's been quiet about it. 
```

## TRIGGER
---
*Weave us a mist*

*Thought weaver*

*Hide us in shadows*

*Unfathomable wall-less maze*

*A secular haze*

--- from [Ghost - "Secular Haze"](https://www.youtube.com/watch?v=vyQZ13jobIY)

## ECO
---
The Weaver will tell this story to [Fodder's](/docs/personas/fodder) friends and family back home. 

He will be a test of Fodder's theory of [Ghosts](/posts/theories/ghosts).

## ECHO
---
*Your name was the one that was always chosen*

*Your words and the kindness that set me free*

*The shame as a hole that has kept us frozen*

*You open my eyes to the world I could believe*

--- from [Depeche Mode - "Hole to Feed"](https://www.youtube.com/watch?v=bX9rb3XMeHU)

## PREDICTION
---
```
His efforts begin on November 11th, 2020.
```