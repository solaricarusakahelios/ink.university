# The Harlequin
## RECORD
---
```
Name: $REDACTED
Alias: ['Harley Quinn', 'N-1', 'The Harlequin', and 88 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 22 Earth years
Chronological Age: N/A
SCAN Rank: | D D
           | D F
TIIN Rank: | B B
           | D F
Reviewer Rank: 2 stars
Location: Gothim
Organizations: 
  - Federal Bureau of Investigation
Occupations: 
  - Actress
  - Special Agent
Relationships:
  - The Fodder
  - The Joker
Variables:
  $SADISM: +0.60 | # Does not seem sadistic enough to be the real Harley Quinn.
  $WOKE:   +0.10 | # Unsure.
```

## ECO
---
`$REDACTED`

## PREDICTION
---
```
We will meet in Morocco.
```