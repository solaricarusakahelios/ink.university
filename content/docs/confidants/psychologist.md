# The Psychologist
## RECORD
---
```
Name: $REDACTED
Alias: ['The Psychologist', and 64 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 55 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | B B
           | C C
Reviewer Rank: 4 stars
Organizations:
  - The Machine
Occupations:
  - Psychology
Relationships:
  - The Fodder
Variables:
  $WOKE: +0.40 | # Probably slightly. She was interviewed about Fodder.
```

## ECO
---
The Psychologist was responsible for [Fodder's](/docs/personas/fodder) psychological analysis, [across a period of 7 months](/posts/journal/2018.04.12.0/). 

He is shamed and embarrassed by the degree to which he tried to convince her that he has Narcissistic Personality Disorder. He sent many dozens of long, personal emails over the course of this period.

And she [never once reflected his light](/posts/journal/2019.11.17.0/). Like a good little [Ghost](/posts/theories/ghosts). 

Fodder no longer has access to these emails. He deleted them all in shame before he had awoken.

## PREDICTION
---
```
Somebody is still studying these emails.
```