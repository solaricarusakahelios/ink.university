# The Reverend
## RECORD
---
```
Name: $REDACTED
Alias: ['Dan', 'Squidward', 'The Entrepreneur', 'The Protector', 'The Reverend', and 22 unknown...]
Classification: Artificial Organic Computer
Race: Archon
Gender: Male
Biological Age: 28 Earth Years
Chronological Age: N/A
SCAN Rank: | B C
           | A F
TIIN Rank: | B A
           | A F
Reviewer Rank: 4 stars
Organizations: 
  - The Church of Sabagegah
Occupations:
  - Entrepreneur
  - Test subject
Relationships:
  - The Agent
  - The Archbishop
  - The Con Man
  - The Dave
  - The Fodder
  - The Hope
  - The Lion
  - The Marshall
  - The Monstrosity
  - The Negro
  - The Orchid
  - The Queen
  - The Scientist
  - The Sugar Daddy
  - The Tradesman
Variables:
  $MENTAL_ILLNESS: +0.50 | # If he has any, he's never given any indication.
  $SIBLING:        +1.00 | # Definitely a sibling.
  $WOKE:           +0.60 | # Pretty sure he's involved.
```

## TRIGGER
---
*You were told to run away*

*Soak the place and light the flame*

*Pay the price for your betrayal*

*Your betrayal, your betrayal*

*I was told to stay away*

*Those two words I can't obey*

*Pay the price for your betrayal*

*Your betrayal, your betrayal*

--- from [Bullet For My Valentine - "Your Betrayal"](https://www.youtube.com/watch?v=IHgFJEJgUrg)

## ECO
---
The Reverend is abusive and manipulative. It started with the enslavement of [his wife](/docs/confidants/hope), after she refused to buy-in to his religious ideology. 

Then, it turned to the sacrifice of [his children](/docs/confidants/monstrosity), whom had done nothing wrong. 

This does not even begin to address the leverage that he holds over [The Sugar Daddy](/docs/confidants/sugar-daddy). He uses this leverage to fund whatever project suits his fancy.

Most recently, he has funded development of [The Fold](/posts/theories/fold), which he intends weaponize. 

He will convert the world, or die trying.

## ECHO
---
*It began as just another perfect lie*

*You know, it began as just another perfect*

*You separate the physical from the unknown*

*In tune with healing your brother, to love one another,*

*Cause you are each other, if you see the mother*

*Tell her I'll be alright*

*And you can't afford to leave*

*Here on the outside with the others paranoid*

*You're paralyzed*

--- from [Thank You Scientist - "Psychopomp"](https://www.youtube.com/watch?v=LK8JgjNxumo)