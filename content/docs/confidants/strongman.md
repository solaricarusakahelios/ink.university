# The Strongman
## RECORD
---
```
Name: Dustin $REDACTED
Alias: ['The Strongman', and 66 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 34 Earth Years
Chronological Age: 12,085 Light years
SCAN Rank: | B B
           | B D
TIIN Rank: | A A
           | A D
Reviewer Rank: 3 stars
Maturation Date: 9/23/2020
Organizations:
  - The Machine
Occupations:
  - IT Operations
  - Strongman
  - Writing
Variables:
  $WOKE: +0.15 | # We suspect that he was connected with Fodder to study him.
```

## TRIGGER
---
When he laughed, saying, "What the heck are you listening to, tribal music?"

## ECO
---
The Strongman is a longtime friend and coworker of [Fodder's](/docs/personas/fodder). 

When he began to distance himself, it taught Fodder everything he needed to know about [the empty echo](/posts/journal/2024.11.03.0/):

The silence means that they understand. They are waiting for us to finish our work.

## ECHO
---
*Dear god, I don't feel alive.*

*When you're cut short of misery will you pray it be the end?*

*Give a look of surprise. Wide eyed to me. Then you'll know just what I am.*

*The scare that triggers your fear.*

*Come know me in a different light.*

*Come know me as God.*

--- from [Coheed and Cambria - "Delirium Trigger"](https://www.youtube.com/watch?v=0NItTBfA_BM)

## PREDICTION
---
```
If it wasn't already obvious, his whole family gets to come. That goes for the rest of them, too.
```