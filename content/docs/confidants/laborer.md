# The Laborer
## RECORD
---
```
Name: $REDACTED
Alias: ['The Laborer', and 9,831 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 46 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 4 stars
Location: Houston, TX
Organizations: 
  - A self-employed lawn-mowing company
Occupations: 
  - The Tradesman
  - Unskilled laborer
Relationships:
  - The Fodder
Variables:
  $MENTAL_HEALTH: -0.80 | # Probably not well. Undocumented immigrant.
  $PLEASANT:      +0.90 | # A very nice guy.
  $RELIABLE:      +0.90 | # A very reliable worker. Even when Fodder is not a reliable employer.
```

## ECO
---
This man has been mowing [Fodder's](/docs/personas/fodder) lawn for 3 years. Doesn't speak much English. 

Fodder respects his hard work. He wishes the man wasn't forced into this career.

## PREDICTION
---
```
He will be the first ASMBarber.
```