# The Pastor
## RECORD
---
```
Name: Robert Houghton
Alias: ['The Pastor', and 1,548 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 45 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | C A
           | B D
Reviewer Rank: 4 stars
Organizations: 
  - The Christian Church
  - The Church of Satan
Occupations:
  - Actor
  - Pastor
Relationships:
  - The Hope
Variables:
  $WOKE: +0.30 | # At least partially. He is exploring foreign ideas with an open mind.
```

## ECO
---
The Pastor is a devil in sheep's clothing. He was born and raised in the Christian religion. He is raising his family in the Christian religion.

However, in the same way that subliminal messaging in rock music reprogrammed [Fodder](/docs/personas/fodder), it reprogrammed The Pastor's son. In turn, it is reprogramming The Pastor himself.

He is drawn to the power this system has to offer. 

## ECHO
---
*Show me*

*The path that leads into your mind*

*Release*

*The thoughts you locked deep inside*

*Show me*

*I'm the one who knew this pain*

*Release*

*Before you fall into this again*

--- from [Demon Hunter - "Ribcage"](https://www.youtube.com/watch?v=0wBuGfEbwGc)