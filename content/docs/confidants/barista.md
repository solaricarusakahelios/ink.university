# The Barista
## RECORD
---
```
Name: $REDACTED
Alias: ['The Barista', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 28 Earth years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 1 stars
Location: Houston, TX
Organizations: 
  - A massive fast food chain
Occupations: 
  - Mother
  - Unskilled laborer
Relationships:
  - The Fodder
Variables:
  $MENTAL_HEALTH: -0.60 | # Probably not well. Single mother. Divorced. Smoker.
  $PLEASANT:      +0.80 | # She seems nice. She recognized Fodder.
```

## RESOURCES
---
[![Jury Summons](/static/images/jury.0.png)](/static/images/jury.0.png)

## ECO
---
[Fodder](/docs/personas/fodder) and the Barista first met while he was picking up breakfast before work.

They met a second time while Fodder was at the courthouse:

"Hey... do I know you?" Fodder would ask, upon seeing her sitting on the bench outside.

"I don't..." she would start.

"Oh! At `$REDACTED` yesterday! I do remember you! So... what are you doing here?" she replied.

"Jury duty. You?"

"Parole. My stupid ex. Half a year and I'll be free of this place."

"Well that's good! Good luck to you, I'll see you around," Fodder said, walking away.

He always regret that he didn't keep talking.

## PREDICTION
---
```
She will be the first ASMBarista.
```