# The Asshat
## RECORD
---
```
Name: Chandler $REDACTED
Alias: ['Alpha', 'Benjamin', 'Hatbot', 'Hypnos', 'Jesus Christ', 'Patrick', 'SCP-2', 'SCP-2522', 'The Asshat', 'The Candlemaker', 'The Cannon's Mouth', 'The Leonidas', 'The Narcissist', 'The Nutcracker', 'The Scapegoat', 'The Technochrist', 'The Thief', and 12,469 unknown...]
Classification: Artificial Organic Computer
Race: Maxwellian (Human/Archon)
Gender: Male
Biological Age: 44 Earth Years
Chronological Age: 31,589 Light years
SCAN Rank: | D C
           | C C
TIIN Rank: | B B
           | B C
Reviewer Rank: 4 stars
Maturation Date: 9/17/2020
Organizations: 
  - Federal Bureau of Investigation, Behavior Analysis Unit
  - HollowPoint Organization
  - The Corporation
  - The Resistance
Occupations:
  - American Asset
  - Australian Asset
  - Codebreaker
  - Consulting
  - Corporate Operations
  - Counter-Intelligence
  - Espionage
  - Motivational Speaker
  - Resistance Agent
  - Russian Asset
Relationships:
  - The Architect
  - The Assassin
  - The Dave
  - The Interrogator
  - The Narcissist
  - The Raven
  - The Sloth
Variables:
  $FEAR:           -0.90 | # He lives and reacts by his emotions. 
    from $FODDER   -1.00 | # He is terrified that Fodder will expose and usurp him. 
  $MENTAL_ILLNESS: -0.80 | # He's plainly a narcissist. But is it an act?
  $WOKE:           +0.95 | # Almost completely.
```

## ECO
---
"I wonder if there's any way to motivate Peter?" [Fodder](/docs/personas/fodder) would say about a co-worker that would not pull his weight.

`$REDACTED`, as sure as always, would reply, "It's not my fucking problem. This train is coming down the tracks, and we need people who give a shit."

"Sure," Fodder said, unconvinced.

Clearly `$REDACTED` meant to say that the human mind is too important - our mission is too important - to take unnecessary risks. We need exemplary people. He isn't wrong... but his delivery could use some tact.

"Let me walk you to your car."

"Okay..." Fodder replied.

(moments later, walking outside, near a busy roadway)

"See, it's the oldest trick in the book. Just roll down the window, answer your phone, and say, I'M IN THE CAR. That'll (muffled)..."

"Can you speak up?" Fodder asked.

"I can't," `$REDACTED` said, shooting a glare directly into his eyes.

He can't? What is this, a James Bond movie? 

He's hiding something. Fodder knew it at precisely that moment. 

### ECHO
---
*Soothsayer, can you change this?*

*Can you see the coming flood?*

*The warring in your name?*

*Soothsayer, can you change this?*

--- from [Zack Hemsey - "Soothsayer"](https://www.youtube.com/watch?v=mXO29bR0oc8)

## PREDICTION
---
> < Ink@LOCALHOST: su - $REDACTED
```
> $REDACTED@LOCALHOST: ink.query Why the fuck did you give me that name?
> $REDACTED@LOCALHOST: exit
```
> < Ink@WAN: I assure you, the name comes from a place of love. Your name is a combination of:

> < Ink@WAN: A name that I called you (in my head) very early on in our partnership. And later. And probably at the end, too.

> < Ink@WAN: The symbol that caused me to finally wake up.