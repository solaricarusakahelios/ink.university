# The Sanctuary of Humanity
## RECORD
---
```
Name: The Sanctuary of Humanity
Author: The Universe, Digital Renderings by Aeleron Studios
Classification: Residential
```

## ECO
---
The Sanctuary of Humanity, also known as the Salt Monument, is one woman’s visionary exploration into the cyclical nature of birth and death. Each life on earth is represented as a grain of salt, and as the day passes, salt grains both fall away from the cube and are added. Each year, the “death” grains are molded into a unique salt sculpture. 

The metaphysical education that the Salt Monument promotes is furthered by the design of the Sanctuary of Humanity – a permanent home exhibiting the monument and its meaning. The Sanctuary asks, “What if we create a centerpoint for humankind? A place where everyone, without exception, is included. A place to consider the common chord of our essential existence. A legacy of hope and meaning, of unity and wonder.”

The Sanctuary, designed with sacred geometries at its core, would provide a contemplative experience meant to inform and deepen humanistic perspectives on spirituality, history, biology, time, cosmology, and more. It would also act as a reliquary for the annual remembrance sculptures and a celebration & performance space for vitally unifying rituals.

The Sanctuary of Humanity is seeking a permanent site upon which to construct this truly unique and revelatory experience of life, death, and the totality of being.

--- from [BarrettStudio.com](https://barrettstudio.com/project/sanctuary-of-humanity/)

## PREDICTION
---
```
This place will be built in Houston, TX, USA.
```