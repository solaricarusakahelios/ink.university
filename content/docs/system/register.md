When the mind shifts an idea into a place where it's not quite dead, but also not very active. Will "trigger" when it is found in the real world.

The register is essentially a "fast-track"; anything flying within 0.05 hops from 0.00 will be activated immediately upon finding a match in reality.

This allows the user to hold many conflicting ideas simultaneously, with minimal mental burden. Decision-making time is left to the moment that a situation arises; not before.