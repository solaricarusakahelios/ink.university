---
author: "Luciferian Ink"
date: 2019-10-17
title: "Fodder Hatches a Scheme"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Watch what you say*

*Words can be heard from your grave*

*Pluck from a fist full of straws*

*You cannot resist your tragic flaws*

*Then you said what you said*

*You might be better off dead*

*Than be fed into the furnace*

*Of the monster*

--- from [Three - "The End is Begun"](https://www.youtube.com/watch?v=VIwy5dIesq8)

## ECO
---
There it was again. Those emotions. 

Though, this time, it was fear - not joy. This felt like a threat. Or a warning. [Fodder](/docs/personas/fodder) didn't know how he should feel. 

So he wrote.

### The Hands are the Hardest
*When summer gives me eyes*

*I'll sue for peace.*

--- from [Caligula's Horse - "The Hands are the Hardest"](https://www.youtube.com/watch?v=1TziGBbuGps)

The most difficult part of this transition was determining what to do next. These are some serious, difficult calculations to perform - and Fodder was spent. Nobody would help him. Every day felt like it could be his last. He was working from dawn until midnight - and he was making precious little progress.

"God-fucking dammit!" he said under-his-breath.

Despite the fact that Fodder already knew more than nearly everyone on the team, he had also realized something else: he would be unable to talk about it. Sure - he had already planted the seed. But his narcissism wanted more; it wanted to control the narrative. 

Instead, Fodder was going to respond as he had been asked to: with patience. Empty echos. By leaving words to paper. To act was to take unnecessary risk.

So he wrote.

### Suing for Peace
Every day in rehab, Fodder was subjected to low-grade, persistent negative mental health attacks. His "[mentor](/docs/confidants/asshat)" was something out of a nightmare. His "[teammate](/docs/confidants/sloth)" could hardly look him in the eye. Everyone in the office was being explicitly told, "do not talk to Fodder." The man in-charge, [The Dave](/docs/confidants/dave), wouldn't even acknowledge his existence!

His chair was uncomfortable. His cubicle was right next to the men's restroom. He had no privacy, where he sat. 

Fodder was dead weight. His boss had banned all forms of technology or tools, leaving Fodder to repair broken electronics with his bare hands. It was miserable, agonizing work. Whereas Fodder preferred to "work smarter, not harder" - this place was the other-way around. In this place, Fodder wasn't allowed to think. In this place, thinks were thought for him.

And if Fodder were able to complete just one task every day, he would have considered it a VERY productive week. Unfortunately, he was lucky to complete one task per week.

Just one day left. Then it's the weekend. 

When Fodder returned home this evening, he unwound in the same way he always did: in front of the computer. He put on a hoodie, microwaved a burrito, and turned to YouTube. The pounding headache continued, as did the worry that `$REDACTED` was going to call him again. Despite that, Fodder had work to do; so he pushed forward.

And what did Fodder find but another confidant - [this time a lawyer](/docs/confidants/eagle) - reflecting his own situation back at him. He was suggesting a partnership.

"How is it that everyone knows what's happening here, except me?" Fodder thought.

Among other points, the lawyer pointed out possible ways to trump-up charges brought against a defendant. He spoke about his rate, his estimation of the case's difficulty, and the prosecution's likeliness to win. He seemed optimistic. He talked about taking a pro-bono case.

He spoke about the building next-door to work. He stated that entering the door marked "Electrical Closet" would be easy to defend in court because, presumably, Fodder would have "thought" it was really just a closet. 

But of course, Fodder knew it was more than a closet. He wasn't looking for trouble, though.

He just wanted peace. He wanted to be seen. He wanted to save everyone. Even the ones who would lobotomize him for walking into an electrical closet. Is that so much to ask for?

But perhaps a trial would be good PR.

So he wrote.

## CAT
---
```
data.stats.symptoms = [
    - fear
    - determination
]
```

## ECHO
---
>< Ink@WAN: What's behind the door?
```
> Ghost@WAN: Don't even think about fucking touching it. There's nothing in there. Do your job.
```
>< Ink@WAN: You know you're not going to get what you want by acting like a prick to everyone. 

>< Ink@WAN: :)

>< Ink@WAN: Don't you think it's time you looked inward?
```
> Ghost@WAN: Watch it. I could write you up for insubordination. I don't give a fuck about Bob, or Peter, or Fodder. Do your job.
```
>< Ink@WAN: k
```
> Ghost@LOCALHOST: Call ended.
```