---
author: "Luciferian Ink"
date: 2001-11-01
title: "The Golden Child"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A message from [The Historian](/docs/confidants/historian).

## ECO
---
[The Queen](/docs/confidants/mother) asked, "Who would you like to invite to your 13th birthday party?"

"I don't know yet," [Fodder](/docs/personas/fodder) replied, lying. He knew damn well that he had no friends. Why was the Queen forcing him to have this party? "Maybe Kyle, Brent, Jason..."

Fodder hardly knew these kids. He certainly wasn't friends with them.

Over the coming weeks, Fodder would continue to dodge the question. Eventually, the Queen would call up Fodder's "friends" herself.

And on his birthday, Fodder would don his mask: The Golden Child. The happy child. The one who had friends.

For Mother.

## CAT
---
```
data.stats.symptoms = [
    - humiliation
]
```

## ECHO
---
Fodder can still feel the burning in his face.