---
author: "Luciferian Ink"
date: 2015-01-18
title: "This Conversation is Over"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*If there's a hole in your heart, stop digging.*

*You've got a scab on your soul? Quit picking.*

*It's as easy as that, and if you're feeling offtrack, just get back ontrack again.*

*Look, you're a quite decent human being. There's just so much that you are not seeing.*

*It's not hard to get, but you get so upset that your walls go up again.*

*And this conversation is over.*

*This conversation is done.*

--- from [Toehider - "This Conversation is Over"](https://www.youtube.com/watch?v=QSc3FOoecuM)

## ECO
---
[Fodder](/docs/personas/fodder) would visit his family one Sunday. The conversation would turn to politics, as it often did when Fodder was with them.

This time, Fodder would make his views on abortion known for the very first time. 

"Get out of my house," said [The Red Queen](/docs/confidants/mother), deadly serious, "I will not suffer that kind of talk."

"This conversation is over."

Stunned, Fodder stood up, defiantly making his way to the door. Mother made her way to the bedroom.

"Fodder, hold on," [Father](/docs/confidants/father) said, "She didn't mean that. Come talk to us."

"If she wants me to leave, I'll leave," he said, putting on his shoes, "I'm so sick of being told what to think."

Father would continue to plead, eventually convincing Fodder to speak with his mother. He would follow Father into the bedroom, where she sat on the bed, crying. 

The White Queen had arrived. 

"Why do you think like that?" she asked, "Why are you okay with killing children?"

"Mom, I'm not!" Fodder exclaimed, "I just don't see it the same way that you do. I have a different [Perspective](/posts/theories/perspective). What good is life, if it's born into misery?"

Unconvinced, she would continue to beg. It was then that Fodder figured, "Now is as good a time as any. They're already upset. I may as well tell them the truth."

He would go on to explain that his sudden change in political stance was, in large part, due to his loss of faith in [God](/docs/confidants/dave). 

"I haven't believed in more than a year," Fodder continued.

The Queen dropped to her knees, sobbing, "Oh God, why oh God, why would you do this to me? Why would you do this to Fodder? Please, I beg of you, God, show him [The Path](/posts/theories/education). Bring him back to you! Can't you see that he is hurting?"

This would continue for more than an hour.

Fodder watched his mother with dispassion. She was so damn weak. She couldn't see past her own emotions to comfort the son before her.

And he had stopped expecting any kind of sympathy long ago.

"We're waiting on you, mother," he thought.

## CAT
---
```
data.stats.symptoms [
    - dispassion
]
```

## ECHO
---
*For once*

*Lets leave the dreaming to someone else*

*Our time has slipped away*

*We're sinking faster, faster, no more play*

*Get ready for the catch*

*I'm at the next step, waiting for a year*

*And all at once, it leaves you breathless*

*With next to nothing but open hands.*

--- from [Emarosa - "The Past Should Stay Dead"](https://www.youtube.com/watch?v=F0fCXsles90)