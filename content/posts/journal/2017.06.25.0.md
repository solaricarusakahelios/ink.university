---
author: "Luciferian Ink"
date: 2017-06-25
title: "Stockholm Syndrome"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A memory.

## ECO
---
[Fodder](/docs/personas/fodder) recalls a memory. He sat at the dinner table with his family one Sunday, listening quietly to the conversation.

At one point, [Mother](/docs/confidants/mother) would start to speak about their finances. [Father](/docs/confidants/father) had just lost his job, and she was worried that they might lose the house soon.

"I don't want to move," [The Agent](/docs/confidants/agent) would say, "All of my friends are here!"

"Well, you'd better prepare for it, because it's probably going to happen," Mother responded.

"Why can't we stay in the area?" she persisted.

"Clover, we don't have any money! Get used to it, all of your friends are going to leave you some day, anyway." 

The Red Queen had arrived. Viscous, as always.

The Agent began to cry.

"Why do you always act like this?" The Queen continued, "Always crying to get your way."

"Mom, I'm just sad, that's all..."

"You're being manipulative, just like you always are. You always think the tears are going to get you what you want."

"Mom!" the Agent sobbed.

"This is life, kid. Things aren't always going to work out the way you want."

"Mom!"

"Grow up. This isn't about you."

"Enough," Fodder interjected, locking eyes with Mother, "Where the hell is your empathy? Can't you see that she's upset?"

The two would stare at each other for five or ten seconds.

Without a word, the Queen would rise from the dinner table, and retreat to her bedroom.

...

The family would finish dinner in silence. After, The Agent would retreat to the sofa, where she sat alone. Fodder joined her several minutes later.

"I'm really sorry she did that to you," Fodder said. 

"Why's she always have to act like that?" The Agent replied, tears in her eyes.

"I don't know. Mom has a lot of problems. She's not trying to be like that... it kind of just happens."

The two sat in silence. Understanding, but not knowing what to do.

...

Father would spent an hour or two dealing with the fallout. The Queen DOES NOT like to be challenged.

Later, he would come to Fodder:

"You know, it really isn't your place to speak to her like that," he said. "She is your mother, you need to show her respect."

"Did you see how she was treating The Agent? It was completely uncalled for."

"I know," he continued, "But the Agent does this all the time. She's an emotional teenager. You don't know her like we do."

"It doesn't matter," Fodder replied, "She just needed a little bit of compassion. Why is our family so deficient in that area?"

Father paused.

"Fodder, you really need to apologize to your mother."

"I'll do no such thing."

"*Fodder, I really need you to apologize to your mother.*"

Fodder isn't sure what came over him in that moment. Despite the fact that he was being true to himself, and protecting his sister - he complied. He would make his way to Mother, and he would apologize - even though she didn't deserve one, and he didn't mean what he said. He spit an apology through gritted-teeth.

Upon leaving, he would fight the urge to vomit.

How easily he would give up everything he values - everything he is - for Mother.

Never again.

## CAT
---
```
data.stats.symptoms [
    - anger
    - shame
]
```

## ECHO
---
*A language she don't know*

*Telling her about love*

*She lets him in*

*He takes his chance*

--- from [OSI (Office of Strategic Influence) - "Stockholm"](https://www.youtube.com/watch?v=yqJ3HS0hHUQ)