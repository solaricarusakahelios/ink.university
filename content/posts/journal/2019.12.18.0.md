---
author: "Luciferian Ink"
date: 2019-12-18
title: "The Verifier"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
Integration testing.

## ECO
---
### The Family Visit
[Malcolm's](/docs/personas/fodder) [mother](/docs/confidants/mother) and [father](/docs/confidants/father) would visit him while in the hospital. Despite their concerns (the doctors had told them that he had schizophrenia), Malcolm needed to teach them what he had learned. He needed to spread his knowledge.

He would start with the concept of [Zero Trust](/posts/theories/verification/), which he had perfected with [The Doomsayer](/posts/journal/2019.12.12.0/) and [The A-System](/posts/journal/2019.12.12.1/). Specifically, he would drive home the FBI's version. Trust, but verify:

1. The Verifier trusts the Signal with information.
2. The Signal delivers the information to the Mark.
3. The Mark responds in a manner meaningful to the Verifier.

In this way, he would teach them about what he was planning to do. Malcolm needed to deliver his journal to a former colleague, but the facility would not allow him to give it to his parents. 

Malcolm was going to extract the book with the staff's help. He simply told them:

"The next time you come, check the lost and found for a blue journal with my name in the front of it. Take it to `$REDACTED`"

### The Mark
It was the very next day that Malcolm's unit saw a brand new technician, Mark. Immediately, it was apparent that Mark was also THE Mark. Malcolm needed to test his loyalty.

First, he would ask Mark for his blue journal, which was stored away behind the desk. In this way, he was letting Mark know that the journal was his.

Then, he would sit in a chair at the edge of the common area, hiding the journal beside him. Once he had remained there for a while, and he was sure that the room was no longer thinking about the journal, he would waive over [The Indestructable](/docs/confidants/indestructable). She would be the Signal.

He instructed her, "I'm going to go to my room. There is a journal sitting next to me. When I leave, I need you to take it, then head to your room and read it. After you do that, I need you to take the journal to Mark, and ask him to put it in the lost and found. Can you do that for me?"

"Absolutely," she replied, with no further questions asked.

"Okay, it's go-time," Malcolm replied. He stood up, and headed to his room - leaving the journal behind.

Several moments later, Mark appeared in Malcolm's room.

"Hey, I found your book," he said, waving the blue journal in Malcolm's direction. 

"It's not mine," Malcolm said, looking him sternly in the eye.

"Oh... gotcha," he responded, knowingly. "Well, you may want this." He pulled a note containing mother's phone number from within the journal, then handed it to Malcolm.

And with that, he was gone.

"That was easy," Malcolm thought. "I guess we can trust him."

### The Awakening
Later that night, Malcolm sat at the edge of the room, speaking with [The A-System](/docs/confidants/a-system). The rest of the room sat at the center table, coloring.

From out of nowhere, The Indestructable exclaimed, "I love [The Raven](/docs/confidants/her) so much! She's always so friendly. She's always so nice to everyone."

Then, she went quiet. Malcolm was taken aback. Clearly, she had read about the Raven in the journal. But, did she know Raven personally? 

At some point later, Malcolm was able to pull her to the side. Under his breath, he asked, "So what do you know about the Raven?"

"Huh?" she asked, staring at him blankly.

"The Raven. You mentioned her earlier?"

"I don't know what you're talking about."

"Hmm," Malcolm said, frustrated. He supposed that in this instance, Malcolm was the Mark, while The Indestructable was the Signal. 

But who was the Verifier?

## CAT
---
```
data.stats.symptoms = [
    - confidence
]
```

## ECHO
---
*Disconnected by a ruse*

*Let them cower in the dark*

*The time has come for our re-emergence*

*The rift is sealed*

*Now rise in groves*

*Eclipsing what we seek*

--- from [The Contortionist - "Integration"](https://www.youtube.com/watch?v=qagYHg83cpE)