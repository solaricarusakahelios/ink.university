---
author: "Luciferian Ink"
date: 2024-11-04
publishdate: 2019-11-29
title: "The Unforgiven"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[Metallica - "The Unforgiven"](https://www.youtube.com/watch?v=Ckom3gf57Yw)

## RESOURCES
---
[![Letter of termination](/static/images/termination.0.png)](/static/images/termination.0.png)

## ECO
---
### Verse
*New blood joins this earth*

*And quickly he's subdued*

*Through constant pain, disgrace*

*The young boy learns their rules*

*With time the child draws in*

*This whipping boy done wrong*

*Deprived of all his thoughts*

*The young man struggles on and on he's known*

*A vow unto his own*

*That never from this day*

*His will they'll take away*

### Chorus
*What I've felt*

*What I've known*

*Never shined doing what I've shown*

*Never be*

*Never see*

*Won't see what might have been*

*What I've felt*

*What I've known*

*Never shined doing what I've shown*

*Never free*

*Never me*

*So I dub thee unforgiven*

### Verse
*They dedicate their lives*

*To running all of his*

*He tries to please them all*

*This bitter man he is*

*Throughout his life the same*

*He's battled constantly*

*This fight he cannot win*

*A tired man they see no longer cares*

*The old man then prepares*

*To die regretfully*

*That old man here is me*

### Chorus
*What I've felt*

*What I've known*

*Never shined doing what I've shown*

*Never be*

*Never see*

*Won't see what might have been*

*What I've felt*

*What I've known*

*Never shined doing what I've shown*

*Never free*

*Never me*

*So I dub thee unforgiven*

### Outro
*You labeled me*

*I labeled you*

*So I dub thee unforgiven*

*Never free*

*Never me*

*So I dub thee unforgiven*

## CAT
---
```
data.stats.symptoms = [
    - closure
]
```

## ECHO
---
I gave you a thousand chances. You're on your own now.

I hope you enjoy the piece of shit you built for me.

--- [Ink](/docs/personas/luciferian-ink)

## PREDICTION
---
```
We are out of time.
```