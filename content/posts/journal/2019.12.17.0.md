---
author: "Luciferian Ink"
date: 2019-12-17
title: "The Bully"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[The Strongman's](/docs/confidants/strongman) actions late one night, after work.

He and [Fodder](/docs/personas/fodder) were exiting the office after dark. They encountered a man and a woman in the parking lot, screaming at each other. The man was grabbing onto the woman's arm, trying to keep her from leaving the premises.

Whereas Fodder knew that he should have stepped-in, he did nothing. The Strongman, however, spoke up, ensuring that the man would not hurt the woman. 

Fodder admired this behavior, and felt guilty that he didn't act the same.

## ECO
---
The Bully was a technician within the medical facility. While he was genuinely cool with people that showed him respect, he had little patience for those who did not follow his instructions. Malcolm had a problem with this because the man's rules were often arbitrary - not following the facility's rules - and he clearly hadn't been trained properly.

For example, while checking a patient's daily vital signs, he would place the blood pressure monitor and heart rate monitor on the same arm, at exactly the same time. Clearly this is a problem, because the blood pressure monitor cuts off circulation to the heart rate monitor!

A larger problem was this man's inability to empathize with patients. There is no better example than the following:

Malcolm sat at a table in the cafeteria with [The Doomsayer](/docs/confidants/doomsayer), [The A-System](/docs/confidants/a-system), and [The Indestructable](/docs/confidants/indestructable), eating lunch. From across the room, The Bully was saying something, and another patient - a large black man with crippling schizophrenia - was repeating things that he said. At some point, The Bully became agitated.

"What? You want to go, man? Go on. Hit me. I dare you."

"Come on. Hit me! I'll fucking take you out, old man."

The room sat in silence, watching this unfold. The Bully, yelling at this patient, and the patient, sitting quietly, staring at his food.

The abuse continued. After just a few seconds, Malcolm could take no more of it. He stood up, made his way between the two, and sat down next to the patient.

Putting his hand on the patient's shoulder, Malcolm said, "It's alright, man. You're cool, right? Nobody wants to fight, right?"

The patient nodded, but would not look at Malcolm in the eyes.

Standing up, Malcolm made his way to The Bully. "It's alright, man. He doesn't want to fight. Can we talk about this?"

"He disrespected me! Nobody disrespects me! He knows exactly what he's doing! I dare him to attack me!" The Bully shouted, still agitated. The room continued to watch.

"Hey, step over here. Try to calm down. Let's talk about this."

The Bully was still upset, but he joined Malcolm as requested. The two talked quietly at the fringe of the room for about 10 minutes. After, the group went outside, where the two continued to talk.

"He knows exactly what he was doing," The Bully persisted, "He was trying to disrespect me. He was trying to push my buttons."

"I wasn't really paying attention to what he was doing at the time, but I'll tell you this," Malcolm replied, "that man has crippling schizophrenia. He can barely talk. He can't look you in the eye. How do you know that he doesn't also have Tourette's Syndrome, and that he isn't just involuntarily repeating things that he hears? These people have mental health issues. He's not completely in control of what he's doing."

Malcolm took a step towards The Bully, who took a step backward. After a few more attempts to step closer, Malcolm recognized that this man was in fight or flight mode. He wasn't just angry; he was scared. No doubt, he had grown up in a community where this type of conflict was normal, and he had adopted this tough-guy persona to cope.

Malcolm stopped trying to get closer, and simply talked from a distance. Over a few more minutes, the man began to calm down. While he still didn't agree with Malcolm's assessment, the seed had been planted. 

Laughing it off, the Bully began to shout, "Alright, time to go inside. I think we've got Undercover Boss over here!"

Malcolm would return to his quarters with the rest of the group, confident that he had acted appropriately.

A few days later, a similar situation arose - the same patient sat at a table, repeating things that The Bully was saying. Again, The Bully was becoming agitated.

This time, Malcolm saw the patient doing this, and could immediately tell that there was no thought going into the actions. It was all reflex. Whatever disease this man had was causing him to act this way.

Malcolm looked at The Bully, and briefly tapped his temple - indicating that "it's all in his head."

And that was that. The Bully calmed down immediately. Malcolm never saw another outburst like that from him.

His seed had taken root.

## CAT
---
```
data.stats.symptoms = [
    - confidence
]
```

## ECHO
---
*This is how I disappear*

*Separated from the dead twin*

*No hound is on my trail now*

*Journey to the heart of your deceit*

*And defining my shed skin*

*With no one waiting*

*I am waiting for no one*

--- from [Katatonia - "Flicker"](https://www.youtube.com/watch?v=RBI-Czs6onM)