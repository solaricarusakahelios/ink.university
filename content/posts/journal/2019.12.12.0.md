---
author: "Luciferian Ink"
date: 2019-12-12
title: "The Signal"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*This disaster binds us absolute*

*A thousand lies you'll tell yourself*

*That no one ever loved you right*

*Ooh, but I would do anything for you*

*The question fits the question mark*

*Your signals crossed, your message lost, haha*

*This wall won't hold forever*

*Your time has come, it's now or never*

--- from [Coheed and Cambria - "Key Entity Extraction I: Domino the Destitute"](https://www.youtube.com/watch?v=QY5mRhB1bak)

## ECO
---
[Malcolm](/docs/personas/fodder) awoke upon his first day without much fanfare. He shuffled through the morning's breakfast, and subsequent group therapy session without much enthusiasm. He really didn't expect to be here long.

Regardless, by late morning, Malcolm found himself sitting at a table, pondering the situation, with fifteen other patients, watching them coloring with markers and crayons. It was then that [The Doomsayer](/docs/confidants/doomsayer) made her way to the chair next to him.

"So, what's your deal?" she asked, "Why are you in here?"

"The FBI brought me in," Malcolm replied. A half dozen heads turned to listen, suddenly interested in his story.

"It's all a big misunderstanding. I'll be out of here in no time," Malcolm assured them. He then proceeded to tell them the story of the gift to his former boss, the police response, and their interest in his website. The room eyed him warily, but the Doomsayer was interested.

"Fucking cops," she said. "We're living in the end times. There's going to be seven years of darkness." Her words would trail off down a path that Malcolm could not follow, but he listened regardless, projecting empathy and confidence as best he could.

"You know, I'm writing about a lot of this stuff. When you get out of here, you should check out my website. It's https://ink.university."

"I really think that all parties involved are working for the greater good," Malcolm continued. "I don't think you need to worry about the end of days. What happens is going to be beautiful. It will be Utopia."

"You're wrong," she replied. "God speaks to me. Only 140,000 people will be going to heaven. If you don't believe in Him, you're going to be left behind."

"I don't know. I don't think any of us know for sure. But I choose to believe that God, if he exists, is better than that."

Again, she trailed off. Without warning, she stood up, and walked over to the phone on the wall. Sitting down, she dialed a number, then waited for an answer.

"7-5-3-6-7-9-2-0," she said to the person who picked up. "I need you to send me a lullaby."

She continued for a few more minutes, speaking in an inaudible tone to the person on the other end of the phone. Finally, she hung up, and returned to her bedroom.

"What was that all about?" Malcolm wondered. "Was that code?"

"Is she sending signals to somebody?"

## CAT
---
```
data.stats.symptoms [
  - curiosity
]
```

## ECHO
---
*Feel like you’re caught up in a proton stream*

*Think you’ve had one too many photon dreams*

*Your lazer only has one setting... stun*

*10 print "bodacious"*

*20 goto 10*

*run*

*The moon, it isn’t shy, it’s just polite*

*You know what time it is, it’s "freezeframed midnight"*

*You’re in the future though you’re from the present*

*Fully charged, never not one hundred percent*

--- from [Toehider - "Wellg*i*vit"](https://toehider.bandcamp.com/album/i-like-it)