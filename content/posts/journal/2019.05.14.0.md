---
author: "Luciferian Ink"
date: 2019-05-14
title: "Search Warranted"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
The name `$REDACTED`, [Ryan](/docs/personas/fodder), and Heather all appearing in the same video.

## ECO
---
It was late one night, approximately 9pm, when Fodder heard a rapping on his front door.

"Open up! This is the `$REDACTED` County Sheriff. We have a warrant to search your house," the officer proclaimed.

"What the...?" Fodder remarked, heart racing. What had he done?

Fodder made his way to the front door, and let the officers inside. He was immediately placed into handcuffs, and taken outside. 

"What did I do?" Fodder asked.

"Allow us to finish our investigation, first. Then we'll tell you."

The officers would spend the next few hours searching Fodder's house. They looked everywhere - including his electronic devices. They were particularly interested in his computer and cell phone. 

Fodder shouldn't have given them his passwords. But he was naive, and he allowed them to search. He was not legally-obligated to do this.

Several hours later - well after midnight - the officers would exit his house, and discuss the situation quietly amongst themselves. They would call somebody on the radio (presumably a superior), before returning to take off Fodder's handcuffs.

"Mr. `$REDACTED`, we apologize for the inconvenience. We received some intel that you may have been involved in something serious, but we have found no such evidence to support it. You may return to your home. Thank you for your cooperation."

"What did the intel say I did?" Fodder asked, curious and nervous at the same time.

"We cannot disclose at this time. But you will receive a copy of the written report in the mail within the next few weeks. It will elaborate further."

"Okay," Fodder replied, still confused.

"In the meantime, we suggest you change your passwords. There's been some strange activity coming from this IP address."

"Shit... really? Okay, I'll take care of it," Fodder responded. "Thanks for the heads-up."

...

Several weeks later, Fodder would receive a letter from the sheriff's office, just as they said he would. Looking it over, Fodder would find the report devoid of any detail. It explained nothing, it made no claims about his behavior, and it listed no specific charges. It simply stated that he had been the subject of an investigation, and that the FBI had been involved.

Fodder would move on with life, chalking this up to a few compromised accounts. Somebody had probably used them to do something shady online.

"God, cyber security is so broken in this world," Fodder said out-loud, "We put all of the onus on the user, instead of protecting identities by-design."

"And this is exactly what happens when an IP address is used to validate an identity. We put innocent people in prison."

## CAT
---
```
data.stats.symptoms [
    - terror
]
```

## ECHO
---
*In the cradle we are helpless*

*But on our feet we are fatal*

*How we evolve and grow into*

*(Twisted beasts with a desire for disorder)*

*Oh! What a terrible, terrible game we play*

*Replacing a pawn for a body and the players*

*Politicians, who say what they need to say*

--- from [The Dear Hunter - "In Cauda Venenum"](https://www.youtube.com/watch?v=rZ2-hwVeO0E)