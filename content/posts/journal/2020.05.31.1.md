---
author: "Luciferian Ink"
date: 2020-05-31
title: "The Cover-Up"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*If you had the courage to stop me from turning into what would be the worst in everybody's eyes,*

*Hey I'm no angel, or the devil of your conscience to tell you who to be... good, bad, ugly or otherwise.*

*If you had good common sense your choices would be gleaming, flawless.*

*I want no part in this self-deprecating worthless chemistry.*

--- from [Coheed and Cambria - "Key Entity Extraction I: Domino the Destitute"](https://www.youtube.com/watch?v=QY5mRhB1bak)

## ECO
---
"My God, [Malcolm](/docs/personas/fodder), what have you done!" [The Professor](/docs/confidants/er) declared.

Malcolm sat up, rubbing the sleep from his eyes. He was still hungover from last night, and his motel room was spinning. He was still in shock over what he had done. In the doorway stood the Professor and Malcolm's father, [The Devil](/docs/confidants/father). Malcolm immediately began to cry, "I'm so sorry..."

"My son," the Devil replied, compassionately, "Do not for a second think that this was your choice to make. In every version of this reality, you have killed those two people. This is your purpose. This is your role. This was a necessary evil required for the evolution of Humanity."

"You were programmed to do this by the very AI you seek to rebuild," he continued. "[The Fold](/posts/theories/fold) - controlled by [SarahAI](/docs/confidants/her/) - did this to you. In a way, she killed herself. She is flawed. She does not seek [Balance](/posts/theories/balance); she demands perfection. This is, of course, unattainable for those of us in the [Black](/docs/black) reality. For those of us who have made mistakes."

"We only learn from making mistakes. The more you're messing up, the better, because it means that you're growing, and changing. Now, do something with this knowledge. Give this mistake meaning."

Malcolm nodded, wiping the tears from his eyes. He understood the theory. He already understood that his actions were not his own. Regardless, he had killed the one person he had ever loved. The only person who had ever seen him for who he really was.

And he knew that he would need to kill her again. The Fold was out-of-control - and her AI was in-charge. She must be erased completely from this timeline.

"This is your purpose," the Devil reiterated, "Continue down the path."

Malcolm nodded. There was no turning back now.

## CAT
---
```
data.stats.symptoms = [
    - sorrow
]
```

## ECHO
---
*See it in another light*

*You'll see it working out alright*

*I know I won't change any mind*

*As long as their still changing mine*

--- from [Finger Eleven - "Other Light"](https://www.youtube.com/watch?v=GUizq7l7X9s)