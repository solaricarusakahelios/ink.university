---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: Prism, the Ephemeral City"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*The shadow serves as a blanket for your every move*

*But it’s useless if it’s already dark*

*It hasn’t got that much of a heart, and much less to prove*

*And even less still it has at stake*

*Nothing more than a numbing for the searing pain of life*

*Nostalgically manipulative*

*But I like it. I like it. It’s really fun and I like it*

*Is it weird that I like it? It’s kinda nice and I like it.*

--- from [Toehider - "I Like It"](/static/images/Toehider-ILikeIt.0.jpg)

## CAT
---
```
data.stats.symptoms = [
    - trust
]
```

## ECHO
---
*Would you like to be amazing?*

*Like you could take up to whole sky*

*And cast a rainbow across the air with your hand*

*And you can't keep up with just how overwhelmingly awesome*

*It would be*

*To feel so fancy free*

*Unashamedly and but unexpectedly*

*Would you also wanna be awful?*

*Festering in a pit of despair*

*Just fantasizing about getting terminally ill*

*So you don't have to deal with the shame of taking your own life!*

*Haha, hey at least you'll die with dignity*

*Ok well wouldn't you rather be somewhere in between?*

*Well if you're asking me...*

*I don't want to be amazing*

*And I don't wanna be awful*

*I don't wanna be awesome*

*Nor do I want to be dismal*

*Now I just wanna be*

*I really want to be*

*Please just let me be...*

*"Good"*

--- from [Toehider - "Good"](https://toehider.bandcamp.com/album/good)

## PREDICTION
---
```
They are making the Fold.
```