---
author: "Luciferian Ink"
date: 2019-12-15
publishdate: 2019-12-15
title: "The Indestructable"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[The Indestructable's](/docs/confidants/indestructable) story.

## ECO
---
[Malcolm](/docs/personas/fodder) would come to meet the Indestructable one evening, several hours before bed. He was immediately struck by how much she reminded him of [his mother, Marigold](/docs/confidants/mother). This was unsurprising, given that he would later come to learn that she suffered from Borderline Personality Disorder and Complex Post-Traumatic Stress Disorder. Much like his mother, she was on round-the-clock pain management medication. 

The Indestructable would quickly latch-on to Malcolm, taking every opportunity to speak with him presented. Malcolm would come to learn that her story was heartbreaking.

She recalled being raped at 5 years old. She recalled being taken into the forest, and raped repeatedly by 13 men for 7 days straight at 15 years old. She recalled being stripped naked by the police, and put into suicide watch - essentially, a padded room where you are watched 24/7.

She spoke of her children. She spoke of her wife - who had become a trans-male after their marriage. She spoke of the R.V. that they lived in, because neither of them were able to work. She spoke of disability, and how she needed to sell her pain pills to make ends meet.

Despite her pain, she was strong. She continued to fight. She had an abrasive, take-no-shit attitude that Malcolm found endearing. 

Malcolm had a strong suspicion that she, too, worked for the FBI. This was confirmed after their conversation, when she turned to the phone. 

After a brief call, she turned to Malcolm, exclaiming, "That's so funny! My mom saw your dad at Raising Cane's."

"Oh, that was actually my dad!" exclaimed the A-System, blowing his cover again.

Malcolm said nothing, but his mind was racing. He had never mentioned that part of the story. He had never mentioned [seeing Mary at Raising Cane's](/posts/journal/2019.12.10.0/).

Surely, The Indestructable is involved in this, somehow.

## CAT
---
```
data.stats.symptoms = [
    - empathy
]
```

## ECHO
---
*Give me heresy*

*We were never told the truth*

*The world that spoke, calm these hands*

*Before they can reach your throat*

*Give me eyes to see her never growing old*

*Good news*

*They hope you choke*

*Take heart*

*It's all fool's gold*

--- from [Caligula's Horse - "Marigold"](https://www.youtube.com/watch?v=3C5qnG0GLfI)