---
author: "Luciferian Ink"
date: 2019-07-11
title: "Fodder Goes to Wonderland"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*I kept these pictures of you*

*And I been telling myself I will be fine*

*But these walls were built to keep us in*

*I never learned to fly*

*I tried so hard to find*

*In these arms you will be safe*

*But in this moment my love*

*You're all that lights my way*

*Home*

--- from [The Butterfly Effect - "This Year"](https://www.youtube.com/watch?v=Jd8idlbvvlM)

## ECO
---
"I'm coming for you, [Mom](/docs/confidants/mother)."

[Fodder](/docs/personas/fodder) sat up, deeply shaken by what he had discovered. He wrote, but it would take many months before he could properly articulate his sense of the events.

Fodder was the one. He had been chosen.

The two of them were going home.

### A Brief Encounter
Fodder was in a horrible state. He had been on a marijuana-induced YouTube bender for the better part of 24 hours, and he was starting to feel exhausted. Still, he was soaking in so much new information - he couldn't stop! That is, until he saw something that really freaked him out.

Psychologists have called it [Alice in Wonderland Syndrome](https://en.wikipedia.org/wiki/Alice_in_Wonderland_syndrome), and Fodder would later come to accept that this was an accurate description of what happened. Still, the experience was burned into his memory.

At approximately 2am, Fodder was watching [this video](https://youtu.be/YJxl35Gd078). As a whole, Fodder found it rather uninspired. However, there is an argument made at 19:00 that he found both compelling and confusing:

*"[Notice] that the [UFO] isn't blinking on and off. I've yet to hear anyone bring this up. Planes and jets flying at night... they just kind of look like a blinking light, in a black sky... this object has no blinking lights."*

#### Confusing
There are a million reasons one could give for an object in the sky to not having blinking lights. There is no reason to attribute this to a UFO.

A lack of blinking lights is a terrible argument.

#### Compelling
Let's assume that the creator of this video was sending a coded-message. Perhaps "blinking" was simply a trigger word, and the viewer was supposed to discover the true meaning on their own. This would fit well with what we know about epistemology - that the best way to change a person's belief is to make them discover something on their own. Make them form a "new connection" in their head.

Well, at 19:35, Fodder made a connection to "blinking." No, the ship did not have flashing lights. It was BLINKING. You can see the eyelashes! The object looks like [a giant rift](/docs/scenes/rifts) in the sky - the other end of a gigantic telescope - and there appears to be somebody looking through it! Blinking.

Suddenly, Fodder was hit in the back of the head by something that felt like a million tiny particles flying straight through him. The impact was so great that his body kicked forward, while his head jolted backwards. Bzzt! It sounded like electricity, though it felt like matter.

Just as quickly as they had entered, they exited through his eyes. With a bright flash of light, and the sound of a camera shutter, the particles were gone. All that remained was the flash of light temporarily burned into his vision.

Fodder decided that it would be best if he went to bed.

### Starblade
Fodder would sit-up in his chair, preparing to retire. However, he would catch something in the reverse-side reflection of his glasses:

Behind him hung a chandelier, with all but 3 bulbs burnt-out. Noticing the reflection, he moved to turn around. Tilting his head slightly, he noticed that the green lights and metal frame came fully into view - and it was nostalgic. 

He was reminded of Splinter Cell, the video game. In the game, Sam Fisher wears a multi-purpose set of goggles (night-vision, infrared, and electromagnetic), which reflect a bright green color. This looked the same.

But the image began to change. Slowly, the shape would morph into something else. Something enormous.

It was a spaceship, unlike any he had ever seen before. It was massive - larger than any single structure on Earth. And yet, it was tiny - only appearing in the reflection of his glasses. The detail was unlike anything he had ever experienced. No amount of computer-generated imagery could produce something of this detail; this was something wholly unique. It was beautiful. Fodder was fascinated and in awe.

That is, until it started to turn. Slowly, the ship began to pivot in his direction. Slowly, it began to come closer. Fodder would soon spot a opening in the front - clearly a cockpit - and he had the strangest feeling that he, himself, were sitting in that cockpit. He felt as though he was connected with this ship via some sort-of cosmic strings. Somehow, he knew that this ship was his - a billion years from now, after he had built it.

This went on for a few minutes - the ship getting closer, Fodder studying in detail - but the intensity became too great. Fodder's heart was pounding. It was too close, and he was not ready to go. He had things he needed to accomplish. He did not want his family to find him - and his house - in the current state it was in. He didn't want to show up unshaven - wearing pajamas, smelling like cigarettes - on an alien spacecraft.

So Fodder went to bed. It would take him many hours to fall asleep this night.

A couple of weeks later, Fodder tried once more. He was able to reproduce the effect and the associated feelings for a second time.

However, he was not able to produce it a third time.

He would speculate:

- A person's position in the universe may enable this phenomena. 
- The chemicals and lack of sleep in his body almost certainly contributed.
- [They were withholding the third visitation until the moment was right.](/posts/theories/verification/)

## CAT
---
```
data.stats.symptoms = [
    - wonder
    - self-doubt
]
```

## ECHO
---
*And all of these precious things*

*Watch over me*

*Wash over me when I sleep*

*To find, to fault, to feel*

*These signs of life*

*There's something here*

*Is it real?*

--- from [The Butterfly Effect - "Signs"](https://www.youtube.com/watch?v=dTgQ04FI4Kg)