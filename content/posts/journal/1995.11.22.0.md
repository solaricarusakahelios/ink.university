---
author: "Luciferian Ink"
date: 1995-11-22
title: "Injustice"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A disagreement with [Mother](/docs/confidants/mother).

## RESOURCES
---
[![Pinewood Derby](/static/images/pinewood-derby.0.jpg)](/static/images/pinewood-derby.0.jpg)

## ECO
---
[Fodder](/docs/personas/fodder) would join the Boy Scouts in first grade. One of his very first projects was the Pinewood Derby, and he was super excited!

He and [Father](/docs/confidants/father) would spend a whole day creating the leanest, meanest machine that's ever graced this planet. Fodder was certain that he would win first place in the race.

Of course, he didn't. Try though he did, quite a few other kids had beaten his score. Fodder was crushed.

Though he tried to prevent it, he began to cry. He was so used to Dad letting him win at every game, he hadn't mentally-prepared for a loss. He didn't know how to deal with the emotions.

His father would grab Fodder by the hair on the back of his head, and drag him into the hallway. Mother would follow.

"Stop it, right now!" [Mother](/docs/confidants/mother) reprimanded, "You're embarrassing us."

"Ow!" Fodder replied, unable to control the tears, "I'm trying..."

"You're not trying hard enough! You should be happy that `$REDACTED` won. You should feel happy for him," she continued.

Father pulled harder on his hair. The tears flowed faster.

"You're going to go congratulate `$REDACTED` for winning, and shake his hand."

"But I'm crying!" Fodder protested.

"I don't care. You're going to do what's right."

Mother would then lead Fodder to `$REDACTED`, so that they could shake hands. Fodder would do so through uncontrollable sobbing and tears, never given the chance to compose himself.

He would never forget the day that his parents humiliated him.

...

Many, many years later, a psychologist would tell Fodder that it's "the parent's responsibility to reflect the child's emotions back at him. They must teach him how to interpret his emotions."

In this instance, Fodder realized that his parents had completely failed him. They should have reflected empathy, and explained the emotions that Fodder was feeling. They should have allowed him to calm down before forcing him to shake hands with `$REDACTED`.

Instead, they punished him for not knowing something he had never learned. It took Fodder almost 30 years to discover what it was that they had missed.

In the meantime, this bad memory became a [core Pillar](/docs/pillars) of his identity.

## CAT
---
```
data.stats.symptoms [
    - foreign emotions
    - humiliation
]
```

## ECHO
---
*Plant a seed, plant a flower, plant a rose*

*You can plant any one of those*

*Keep planting to find out which one grows*

*It's a secret no one knows*

--- from [Hanson - "MMMBop"](https://www.youtube.com/watch?v=NHozn0YXAeE)

## PREDICTION
---
```
On 8/7/2020, a pull request was made to change "Hansen" to "Hanson." Upon double-checking, we found that it had been changed across the entire Internet. 

Ink did not make a mistake. He double and triple-checked this spelling when he wrote the original article.

This is an example of the Mandela Effect being faked by The Machine.
```