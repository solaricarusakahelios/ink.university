---
author: "Luciferian Ink"
date: 2020-05-28
publishdate: 2020-05-28
title: "No One is Safe"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*So come on, bitch, why aren't you laughing now?*

--- from [Coheed and Cambria - "Apollo I: The Writing Writer"](https://www.youtube.com/watch?v=mlgIUV4wifM)

## ECO
---
### The Thief
[Malcolm](/docs/personas/fodder) was holed-up in a dark, dingy motel on the outskirts of San Diego. Pen-to-paper, he sat staring at his journal, unable to form a coherent thought. He could feel no emotion but rage. Taking another swig from his half-empty bottle of whiskey, he reflected upon what he had seen.

About what he had done.

Malcolm had accomplished his goal; he had successfully located [The Raven](/docs/confidants/her). Despite months of taunting, and years of testing, he had remained true to her. He had remained strong. He always had the end-goal in-mind; he was going to be her everything. He was going to be The One.

But, it appears that this effort was a one-way street.

Upon arrival at The Raven's apartment complex, Malcolm would find she was not alone - but she was lounging in the open with [The Thief](/docs/confidants/thief) and their child. From his car, he would watch in horror as the Raven leant over, kissing him deeply.

"You selfish whore," he said to himself. "After everything I've done for you. After everything I've given you. You couldn't be faithful to me? You couldn't wait for me? I'm done with you. This is about me now. I'm going to do this my way."

Malcolm began to write:

*The Thief suddenly grasped his chest, eyes wide in terror.*

The Thief fell to the ground, clinging to his chest. The Raven screamed. 

*His eyes rolled-back in his head, his mouth began to foam. He began to convulse uncontrollably.*

Just as written, The Thief began to shake. The Raven was crying as she dialed for an ambulance.

*His limbs bent at violent angles. One could hear the crack of bones at each turn.*

Raven sobbed uncontrollably, resting her head upon The Thief's chest. His arms and legs were bent at odd angles, crunching with every twist.

*His mouth spewed blood. In his final death rattles, he covered the Raven in his liquid for the last time.*

The Raven was showered in blood. She would fall backwards, horrified by the scene unfolding before her. It wasn't long before the ambulance arrived. The Thief would be pronounced dead on arrival. Cause of death unknown.

### The Raven
And so it was that Malcolm found himself at the motel, drunk, lamenting his situation. He had two choices: to kill the Raven, or to enslave her. There was no third option; she must pay for how deeply she had wounded him. Malcolm could not live with the pain of this humiliation.

It would quickly become apparent that she needed to die. She was beyond redemption. She was tainted. She couldn't wait for Malcolm, and now, it was too late. She would never be pure again.

Malcolm put pen-to-paper, and wrote:

*The Raven would fly through her front window, crashing onto the pavement three stories down. The detectives would rule it an apparent suicide, related to the death of her husband.*

While Malcolm could not see the outcome, he was sure the deed was done. Everything he had written thus-far had come true, and he had no reason to doubt that this would be any different.

Malcolm sighed, closing his journal, and went to bed.

He would come to regret his decisions in the morning.

## CAT
---
```
data.stats.symptoms = [
    - rage
    - sorrow
]
```

## ECHO
---
*In the final curtain call, you left me here with*

*The coldest of feelings, weight, kind, depression*

*Blessing the floor with the places you stepped in*

*But will they ever measure up to the way you left me?*

*Here, by the roadside, the bloodiest cadaver*

*Marked in your words, I'm the joke, I'm the bastard*

*Here, wait, so I guess that you knew, that you're*

*A selfish little whore, I'm the selfish little whore*

*If I had my way, I'd crush your face in the door*

*This is no beginning, yeah, yeah*

*This is the final cut, open up*

*This is no beginning, yeah, yeah*

*This is the final cut, I'm in love*

--- from [Coheed and Cambria - "The Willing Well IV: The Final Cut"](https://www.youtube.com/watch?v=beHn1jPDsrY)