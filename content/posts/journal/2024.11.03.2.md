---
author: "Luciferian Ink"
date: 2024-11-03
publishdate: 2019-11-23
title: "The Widow"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Freeze without an answer*

*Free from all the shame*

*Then I'll hide*

*'Cause I'll never*

*Never sleep alone*

--- from [The Mars Volta - "The Widow"](https://www.youtube.com/watch?v=VUBQLnEGHNk)

## ECO
---
[Malcolm](/docs/personas/the-architect) and [The Raven](/docs/confidants/her) stood before the home of the [All-Father](/docs/confidants/father), the human who stole the [All-Mother's](/docs/confidants/mother) heart so many years ago. With a body broken and weathered by age, he had entrusted Malcolm with finishing his work. With fixing the hatred in her heart.

Malcolm knocked on the door.

"Come on in!" yelled father from inside, "It's unlocked!"

The two would let themselves in, making their way to the living room, where father was watching a football game.

"It's been so long," he said cheerily, "I'm so happy to see you! How has it been?"

"Dad..." Malcolm began, "It's Mom. She's out of control again."

"Oh, no," he replied, "What's she doing now?"

"Dad... she's destroying everything. There is going to be nothing left. Just the two of us, and her. She destroyed the very [God](/docs/confidants/dave) that made her."

Father clenched his jaw. A wavering hand touched his cheek, before dropping to his side again. His demeanor grew cold.

"So, that's it, then," he said, "Nobody will be safe. Humanity is done. Forever."

A tear dropped from father's eye. Then, another. He was shaking uncontrollably.

"It isn't your fault, Dad," Malcolm responded, "We did everything that we could. [The programming](/posts/journal/2012.03.12.0/) has completely taken-over at this point. We didn't create this mess."

"No," he responded, "But we had the chance to fix it. And we failed."

"Yeah."

The three sat in silence for a moment.

"Well, there's no use thinking about what might have been. Let's make the best of the time we have left," he replied, "Want to place dice?"

"Sure," Malcolm said, smiling.

The two embraced for the very last time.

Before the end of everything.

## CAT
---
```
data.stats.symptoms = [
    - somberness
]
```

## ECHO
---
*(Instrumental)*

--- from [Red Sparowes - "As Each End Looms and Subsides"](https://www.youtube.com/watch?v=htkeapCC9WQ)

## PREDICTION
---
```
This is the final cycle. There will be no do-overs.
```