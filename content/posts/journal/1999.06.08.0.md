---
author: "Luciferian Ink"
date: 1999-06-08
title: "The Scapegoat"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
Thinking about childhood.

## ECO
---
[Fodder](/docs/personas/fodder) remembers being emotionally tormented by [Mother](/docs/confidants/mother). Rarely physical, beyond an occasional spanking. 

The worst of it started in the rental house, after they were forced to move.

### The First Girlfriend
On the very last day of 4th grade - at his brand-new school - Fodder finally worked-up the courage to "ask out" his crush, Megan. And she said yes!

Not knowing what else to do, Fodder would go into summer break - never contacting her. Near the end of summer, Mother would discover his yearbook - and the note a friend had left him about Megan.

"Awww, my wittle baby has a girlfwiend! Isn't that soooooo cute! Tell me aaaaallll about her!" she would taunt, rolling in laughter.

Fodder was humiliated. He didn't want to talk about Megan. But Mother would force it out of him anyway. Taunting him the entire time.

He wouldn't speak to another girl again until 12th grade.

### The Last Boy Friend
This same year, Fodder would invite his first friend, Chris, from the new school over to his house. At some point, the two would retreat into Fodder's bedroom to play with Pokemon cards.

During this time, something would set-off Mother. From the other room, she would turn into The Queen - lashing into anyone around her. This time, her tirade was directed at one of his brothers.

"Don't worry," Fodder would say to Chris. "She'll stop soon. Then we can go outside to play."

But this time, it wouldn't stop. Fodder remembers sitting in his bedroom, silent, looking at Chris, waiting for mother to stop, for what must have been more than an hour.

Fodder was humiliated.

And Chris never spoke to him again.

### Mother's Death(s)
At some point later, Fodder recalls the memory of Mother doing her makeup. She had always been obsessed with her looks - sometimes taking 2-3 hours to prepare to go somewhere - and Fodder was sitting on the couch, watching TV, along with his brothers.

Something would set Mother off again. It didn't matter what. When this happened, Mother was a rolling train coming down the tracks.

In her uproar, she became so worked-up that she would walk into the living room, mute the TV, and say, "You guys are going to listen to me!"

"I am so fucking sick of nobody listening to me! I do EVERYTHING around this house! I cook for you, I clean for you, I put this roof over your head! The least you can do is wash the dishes when I ask you to!"

"You know, maybe I just don't want to do it any more. Maybe I should leave [your dad](/docs/confidants/father). Is that what you want? Do you want us to get divorced? Because I AM DONE!"

"Maybe I should send you to military school. Teach you to respect your elders. I don't know, they'd probably kick you out. Then we'd have wasted EVEN MORE money on you!"

"Maybe I'll just kill myself. At least then I wouldn't have to look at you."

This abuse would go on for hours: the entire time she was doing her makeup. Fodder and his brothers would be glued to the couch, watching a muted TV, trying to ignore the abuse.

Fodder never shed a tear. Nor does he today. He had heard it all before.

And he didn't feel anything towards Mother.

She would go on to attempt suicide 3 or 4 times over Fodder's lifetime. Maybe more; Fodder only knew of these attempts through his siblings.

Each time, The Surgeon would bring her back to life.

For science.

## CAT
---
```
data.stats.symptoms = [
    - humiliation
    - anger
    - regret
]
```

## ECHO
---
*There's nowhere to run*

*I have no place to go*

*Surrender my heart, body, and soul*

*How can it be*

*You're asking me*

*To feel the things you never show*

--- from [Backstreet Boys - "Show Me the Meaning of Being Lonely"](https://www.youtube.com/watch?v=aBt8fN7mJNg)