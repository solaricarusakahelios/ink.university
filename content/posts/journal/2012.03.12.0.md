---
author: "Luciferian Ink"
date: 2012-03-12
title: "The Programming"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*Embraces the chi and he praises the weeds*

*Holding out for the day he can face them*

*And every day that he trains is a slap in the face to the gods*

*Goddamn*

*Shutting out politics*

*Viewing beings as equals*

*Ain't it funny how that makes sense?*

--- from [Periphery - "Reptile"](https://www.youtube.com/watch?v=fQQxhyhdg-w)

## RESOURCES
---
[Malcolm's resume](/static/reference/fodder_resume.pdf)

## ECO
---
[Malcolm's](/docs/personas/the-architect) career was a long and storied one. While it was by no means easy, he would later come to learn that he never really had to put effort into getting a new job. They simply fell into his lap.

Malcolm never had any certifications. He never finished college. He simply followed his interests, and learned what he wanted to. When opportunities arose, he took them.

In this way, [The Machine](/docs/candidates/the-machine) programmed Malcolm to be [the perfect Architect](/docs/personas/the-architect). It was never his choice. Malcolm simply followed the path laid-out before him.

### The Healthcare Company
While Malcolm had already worked several entry-level tech jobs, his true career started with United States Surgical Staffing. 

USSS was a small healthcare company, with an outsourced IT managed services provider (MSP). Malcolm worked for this MSP.

One day, just several months after this client was brought on-board, Malcolm was assigned as the "dedicated resource" to this customer. He was so excited; this was his very first client!

He would visit USSS just a few times, before being presented with an unexpected opportunity:

"Malcolm, please come in my office," said the CEO. "Close the door behind you."

Malcolm did as he was instructed.

"Please sit down," the CEO continued. "You know, I think you've done great with our IT work. How would you like a job?"

"I, er..." Malcolm floundered. He was not expecting this at all.

The two would discuss the job requirements in detail, before Malcolm would eagerly accept. How could he pass this up? It was a major step in the right direction; he would be moving from a help desk technician to a systems administrator!

Of course, he would be stabbing his current employer in the back - and taking their client with him. He felt bad, but it was a sacrifice that he was willing to make.

...

Fast-forward several months, and Malcolm was happily working at USSS. He would come to learn several important details about his employment there:

1. Having just sold the company to private investors, the CEO was not authorized to hire Malcolm in the way that he did. He had undermined the board of directors.
2. The company had to pay $40,000 in court for violating their contract with the MSP. They were not supposed to hire employees of the MSP.
3. For these reasons, and more, the CEO was eventually terminated. 

This knowledge lit a fire under Malcolm. The company had gone out of their way to hire him, then dealt with the fallout from doing so. He would prove to them that he was worth it. 

He would become the Architect that they needed.

### The Cyber-Security Consultancy
After five years with the healthcare company, Malcolm was ready to move on. The company had grown stagnant, and his wages had not kept up with his increase in responsibility.

He placed an application with several companies. One of them, Crowdstrike, immediately contacted him. Their "Identity and Access Management" business unit was seeking an administrator to assist with their Windows clients.

After just one phone interview, Malcolm was told that he had the job. Corporate would later force them to have a traditional interview, "just as a formality."

Malcolm barely had to try. The job simply came to him.

...

After several years with this company, Malcolm had come to learn that they had more or less left him to his own devices. He was working from home most days, only going into the office one or two days a week. Save for a few, minor projects, he was given freedom to learn what he wished.

So, he would focus on DevOps - the integration of development and IT operations. He would focus on automation. He would focus on Identity Management.

And he would soon come to learn that these skills were exactly what he needed to save the multiverse.

### The Prison System
Malcolm was "pushed" into the final job out of necessity. Crowdstrike had assigned him to a new customer, and that customer was requiring a drug test - which he wasn't going to pass. So, he exited the company with grace.

Within the first week, Malcolm was contacted by "Lock 'em Up Technologies." He did not apply; he was chosen.

The very first phone call was 5 minutes of speaking, and 55 minutes of listening to `$REDACTED` brag about himself. At the end, Malcolm was offered the position of "Senior DevOps Engineer" - which he quickly accepted.

He didn't have to do a damn thing. The job simply fell into his lap.

[And thus began his slow descent into madness.](/posts/journal/2019.06.24.0/)

## CAT
---
```
data.stats.symptoms = [
    - understanding
    - gratitude
]
```

## ECHO
---
*Please accept this life we've prepared for you*

*All of the ways we've taken care for you*

*Is this not something that they expected you to do?*

*How could you not want your destiny handed down to you?*

--- from [Mushroomhead - "The Flood"](https://www.youtube.com/watch?v=2PM_i3LHbYw)