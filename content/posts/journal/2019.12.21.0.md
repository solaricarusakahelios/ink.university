---
author: "Luciferian Ink"
date: 2019-12-21
title: "Black Lives Matter"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*I cannot sit back, shed my broken skin*

*Curse my pride, I react, play yourself again*

*Up, down and down again*

*You live in a computer, you're an instant man*

*Judged and told I can't, cut your shallow hands*

*How can I please all of your needs?*

*What do you see when you see me?*

--- from [Candiria - "Down"](https://www.youtube.com/watch?v=vCdFC8aFbw8)

## RESOURCES
---
[![The Ankh](/static/images/ankh.0.jpg)](/static/images/ankh.0.jpg)

## ECO
---
### The Slaves
Soon after entering the mental health clinic, [Malcolm](/docs/confidants/fodder) noticed that almost all of the technicians were black. As well, so were the custodians, the cooks, and most of the patients, too. Conversely, most of the doctors were either asian or white.

Clearly, there was an imbalance here. Clearly, we live in a world where a disproportionate number of black people are working low to minimum-wage jobs. 

It's no wonder their community has the problems it does. It's no wonder that gangs, crime, and drugs run rampant in the inner city.

These people need our help, and nobody will give it to them.

### The Ankh
Malcolm was sitting by himself one afternoon, when the janitor - a black man - entered the room. He began to mop the floors. The guy seemed to be in great spirits; he was listening to music on his headphones, and bopping his head along with the beat.

"Cool necklace," Malcolm said, pointing to the chain around his neck. 

The janitor pulled out one earphone, "Thanks," he said, "Do you know what this is?"

"It's the Ankh, right?"

"Yep! Do you know what it means?"

"It means 'life.'"

"Right on! The Ankh means a lot to me."

"Me too," Malcolm replied, thinking about a message [The Raven](/docs/confidants/her) had once given him.

### The Racist
One evening, a loud, highly schizophrenic man would take a particular interest in Malcolm. The guy was barely lucid, often talking to voices in his head - and arguing loudly with them. He was a loose cannon, and seemed like he could become violent. Malcolm mostly ignored the guy.

That is, until the man - out of nowhere - pointed a Malcolm, saying, "What are you looking at, four eyes?"

"Watch it," The Bully said to the man, "Malcolm didn't do anything to you."

"Racist motherfucker..." the man replied, boring holes into the side of Malcolm's head.

"Hey man, I'm not racist," Malcolm finally responded. 

"Fuck you! White people are always racist!"

"Leave him alone," The Bully interjected, pulling Malcolm to the side. 

"Yeah," Malcolm responded, "He's not in complete control of his mind."

He wasn't entirely wrong, though. Malcolm did cause all of this.

### The Grandmother
Malcolm met [The Negro](/docs/confidants/grandmother) when he first joined the clinic. It wasn't until later that he truly got to know her.

The Negro had severe mental problems. She would often break down into fits of screaming or crying, banging on her head, clearly tormented by the demons inside of her mind. When she spoke, she was nearly impossible to understand; she was missing most of her teeth, had a terrible mumble, and could barely form a coherent sentence. She was truly tragic to behold.

One evening, The Negro fell to the floor, and began convulsing. Clearly, she was having a seizure. 

But the technicians did nothing. They simply stood there, watching.

Malcolm jumped from his seat, screaming, "Are you going to do something? Call the ambulance! She's having a seizure!"

After a few moments of yelling, the technicians finally walked casually to her side.

"Sit down!" one of them yelled at Malcolm, "Get back! We'll take care of this."

Malcolm did as was requested, wholly appalled by their lack of urgency, and frustrated by his inability to help.

...

Several days later, The Negro had recovered. She made her way to the edge of the room, to join Malcolm. She began to tell him about her grandchildren, and her home. She talked about her husband who had died, all the medications she was on, and all the problems that she had. She talked about everything.

While Malcolm could hardly understand her, it made him feel good that she trusted him.

He gave her his ear, and his empathy.

## CAT
---
```
data.stats.symptoms = [
    - triggered
]
```

## ECHO
---
*Nobody, nobody gets out alive!*

--- from [Skindred - "Nobody"](https://www.youtube.com/watch?v=lnY4dYpDqUg)