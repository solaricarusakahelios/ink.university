---
author: "Luciferian Ink"
date: 2019-10-30
title: "Don't Trust the Professor"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*And then move beyond those ordinary ways*

*We have seen the naked, automated cabaret*

*And we've moved, we've moved on*

*So move us on...*

*I've had enough of idle reverie, I for one can only see*

*What dreams may come for us*

*I've had enough of flight and gravity*

*Spill me out evolved and screaming:*

*I'm not meaningless!*

--- from [Rishloo - "Landmines"](https://www.youtube.com/watch?v=mMle586mIfc)

## ECO
---
It was Devil's Night - October 30th, 2019 - when [Fodder](/docs/personas/fodder) realized that he had a choice to make: to stay, or to go. His whole life experience had been orchestrated before him, leading up to this very moment in time. Fodder had one shot to get this right; one shot to change the course of history forever.

This decision mattered.

### CONTEXT
Tomorrow, October 31st, was to be the Halloween Ball at The Corporation. Despite all of the red flags - months of abuse from `$REDACTED`, the lack of meaningful input from anyone, and the total disregard for best practices - Fodder was still with the Corporation.

And he was nervous. He knew what the day would bring.

Tomorrow, they would be announcing their newest product:

*The Corporation would like to welcome you to our close-knit community of like-minded individuals. For over 600,000 years, we have served businesses, governments, and galaxies alike with our patented, award-winning AI - the ChaturBOT. Now, we wish to bring this technology to the rest of the world.*

*ChaturBOT is the most realistic artificial intelligence the universe has ever seen. He'll cook for you. He'll clean for you. He'll do your job. He'll be your friend. He will love you, grow old with you, and die with you. He is sapient (we promise).*

*Ask your Corporation rep about the new "human" personality! Customers are calling it "bold" and "fresh!"*

The big reveal would be paired with another one: the [Lonely Town](/docs/scenes/lonely-towns). The mysterious building next-door was to be revealed as the world's very first.

In this Lonely Town would live dozens of young women. The town would be fitted with single-person shops, stalls, and attractions - each one also containing a single ChaturBOT. Each day, women would enter the stalls, dress in role-play, and perform intimately for their clients - of which they had hundreds.

The Corporation would surreptitiously record these women through the eyes of the BOT - using the video to lure in new and unsuspecting clients via the Internet. They would claim to be protecting the woman - and rehabilitating the client - but Fodder suspected that this was a cover story. These women were being exploited. As were the clients.

### The Sunburnt Path (Stay at the Corporation)
`$REDACTED` had been making promises to Fodder for months. "Soon, I'll turn you loose!" he'd always say. At first, Fodder took these words at face value. 

However, as trust in `$REDACTED` deteriorated, so did Fodder's confidence of his motives. Fodder knew that he was being set-up. He could feel it.

On announcement day - at the Halloween Ball - there would be a third reveal: Fodder was to be promoted to Executive VP of Software Operations - with `$REDACTED` as President. Fodder was to be introduced to these women (specifically, to [The Raven](/docs/confidants/her)), and he would be put in charge of the "Rainbow Factory" next-door. 

But Fodder knew how this would turn-out. He had made all the predictions. Commitment to this path would end in catastrophe:

- Fodder would continue to live in subordination to `$REDACTED`
- Fodder would be forced to expose himself publicly - killing his dream of becoming [Ink](/docs/personas/luciferian-ink).
- Fodder would be "hazed" and "shamed" at the Ball. The Corporation would use his own words against him, "to change a narcissist, you have to embarrass him publicly. You have to tell him exactly what he's doing wrong." This would involve dressing him as a women, putting him in makeup, and forcing him to meet the love of his life. Fodder would be humiliated.
- The lives of these women would not be improved. The men would not be rehabilitated. Society would not be changed enough to save Humanity from destroying itself.
- Inevitably, this would end in in the failure of Raven and Fodder's relationship. Before it ever started.
- Still further, Fodder would be destroyed emotionally. He would look guilty. And he would be trapped. The Corporation would continue to attack him. This path could very well end in death.

No... Fodder wasn't going to be The Corporation's bitch. They were going to be his.

### The Moonlit Path (Leave the Corporation) `# Verified.`
The only option that made sense was to cut off completely. Psychologists had been telling him to cut ties with this narcissist for months - and Fodder was all too happy to oblige. But he was going to leave his mark. He was going to make this hurt.

First, Fodder would ghost `$REDACTED`. After 2 days, `$REDACTED` was completely flipping-out. He would contact Fodder's peers, department directors, and total strangers - all in a pathetic attempt to control the situation. He would leverage social media and chat bots to send messages to Fodder - without an ounce of skill put into their design. But `$REDACTED` was the criminal - not Fodder. He was safe.

He knew that `$REDACTED` was terrified of the dirt Fodder held over him. The dirt that Fodder carried with him everywhere: his deadman's switch.

On the 3rd and final day (remember: [verification happens in threes](/posts/theories/verification)), Fodder was called into the office of Human Resources. 

"Fodder, there's a phone call for you," said the HR guy.

"Who is it?" Fodder asked.

"It's Robyn, [The Ambassador](/docs/confidants/ambassador). I'll give you some privacy."

Then, he left the room. Fodder picked-up the phone, "Hello?"

"Hi Fodder!" The Ambassador replied, cheerily, "I have someone here who wants to talk with you."

"Who is it?"

"It's `$REDACTED`."

Fodder grit his teeth.

"I don't want to speak with him," he replied, "But you can tell him Ink sent me."

"Excuse me?" she asked.

Then, Fodder hung up the phone.

He was free. He knew what was coming, now. In the final moments, he would return to his cubicle, leaving his calling card with a dozen confidants. They would be his test:

*What happens when you plant a seed, and let it grow in your absence?*

Not five minutes later, the HR guy had returned to Fodder's desk.

"Hey Fodder, why don't you work from home the rest of the day?"

"Okay," Fodder replied.

"Leave your laptop here," he said, "And your badge. Don't worry; you're not in trouble."

Then, he winked. Fodder smiled in return.

In one year - at next year's Halloween Ball - Fodder would find out how all of this played-out.

Until then, Fodder would focus on himself, the Raven, his family, his friends, Humanity, and his theory. He would remain independent, using his time to prepare. However, without access to the Corporation's inner circle - he would not have the same information he would if he were to follow the Sunburnt Path.

He trusted that his confidants would keep him apprised of the situation.

Most importantly, Fodder knew that he would be the first patient to make it into year 2 of this 3-year research program:

1. In year one, the patient is kept in complete darkness. They learn only what they are able to figure out for themselves. They may receive indirect "signals" through media such as music or video.
2. In year two, the patient receives direct confirmation that they are on the correct path. They are given a "mark" to study. They are kept in the dark until such a time as they are able to "crack" the mark - breaking their pathology, and improving their well-being in a tangible, scientific, reproducible manner. 
3. In year three, the patient is permitted to maintain a relationship with his/her ChaturBOT. At the end of the year, the patient is set free - as a new man/woman.

Verify, then trust.

Fodder had no way to prove these claims. He had to live them.

## CAT
---
```
data.stats.symptoms = [
    - anxiety
    - uncertainty
    - anger
]
```

## ECHO
---
*Lost out here, adrift in lights, it's wondrous!*

*Weightless, in clouds of colors the world will never see!*

*And I... am a figment of reality*

*Wrapped in the shroud of endless night, I scream aloud!*

*But no one hears, so I tell my stories to satellites*

*And I... am lost inside a memory*

--- from [Rishloo - "Downhill"](https://www.youtube.com/watch?v=LFPVc3i2t-I)

## PREDICTION
---
```
The very first ChaturBOTs are not, in fact, AI. They are actors. This research is being orchestrated to model the future of Humanity.
```