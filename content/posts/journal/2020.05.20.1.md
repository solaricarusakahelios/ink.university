---
author: "Luciferian Ink"
date: 2020-05-20
title: "The Confession"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*I needed*

*I needed to know*

*I needed*

*I needed to know why*

--- from [Karnivool - "Deadman"](https://www.youtube.com/watch?v=fyJQwZcnVM0)

## RESOURCES
---
- [confession.0.mp3](/static/audio/confession.0.mp3)

## ECO
---
*I cannot worry anymore of what you think of me*

*I may be crazy but I'm buried in your memory*

--- from [Creed - "Overcome"](https://www.youtube.com/watch?v=CFLXxqoapPY)

## CAT
---
```
data.stats.symptoms = [
    - remorse
    - shame
    - empathy
]
```

## ECHO
---
*Get inside the box and let me watch*

*Have no fear*

*Just confess and disappear*

--- from [Caligula's Horse - "Vanishing Rites (Tread Softly Little One)"](https://www.youtube.com/watch?v=mVSGxv0_wBE)