---
author: "Luciferian Ink"
date: 2019-12-23
title: "The Lawyer"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## ECO
---
Just before Christmas, [Malcolm](/docs/personas/fodder) met privately with a public defender. The man explained Malcolm's situation clearly, telling him that the hospital would be taking him to court in order to force medication. He suggested that it would be easier to just take the medication.

Malcolm sighed, "I'm stuck. I don't know what to do. I feel like I have to choose between taking the easy way out, or taking the right way out. I feel obligated to fight this."

"I understand," the lawyer said, knowingly. "With that in mind, you should know that there are a lot of eyes on this case. We intend to call-in a lot of witnesses."

Malcolm's demeanor instantly brightened, "That's fantastic news. Thank you so much!"

"You're welcome," he replied. "Now stay strong. We're going to get you out of here."

## CAT
---
```
data.stats.symptoms = [
    - trust
]
```