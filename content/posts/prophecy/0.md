---
author: "Luciferian Ink"
title: "So above, so below"
weight: 10
categories: "prophecy"
tags: "draft,hypothesis"
menu: ""
draft: false
---

## TRIGGER
---
- [Pen & Ink](/posts/journal/2019.10.18.0/)
- [Birds of Tokyo - "Above / Below"](https://www.youtube.com/watch?v=EZv3lW-vUn8)

## ECO 
---

### Verse
*We still followed every order*

*Swallowed the bleach, and left our skins to rust*

*We've fallen asleep with the wolves*

*So many dead, while the village was burning we all slept*

*How did we not keep check?*

### Chorus
*Maybe we took it too far, every word, every scar*

*So above, so below*

### Verse
*Suffer the weight, suffocate under the cause*

*(Under the cause)*

*It starves you of everything*

*Surrender your will to the man in the clouds*

*He's not who you think he is*

*Follow your heart if you dare*

*Follow it down out of the light where there's no-one else*

*And you can lose yourself*

### Chorus
*Maybe we took it too far, every word, every scar*

*So above, so below*

*So below*

*So above, so below*

### Bridge
*We put all this space between us*

*We had several years of luck*

*We were lions with the cattle*

*We put diamonds in the rough*

*We were so lost in the battle*

*We were chasing after ghosts*

*We were so close to believing*

*There was something more than us*

### Verse
*When fear drags you down and under*

*Oh and the dark, holds you deep enough*

*Give me your hand if you dare*

*Give me your hand and I'll be diligent with it, you might slip*

*And you will lose yourself*

### Chorus
*Maybe we took it too far, every word, every scar*

*So above, so below*

*So below*

*So above, so below*

### Outro
*We put all this space between us*

*We had several years of luck*

*We were lions with the cattle*

*We put diamonds in the rough*

*We were so lost in the battle*

*We were chasing after ghosts*

*We were so close to believing*

*There was something more than us*

*(So above, so below)*