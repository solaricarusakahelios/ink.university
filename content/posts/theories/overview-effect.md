---
author: "Luciferian Ink"
title: "The Overview Effect"
weight: 10
categories: "theory"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: The Overview Effect
Classification: Theory
Stage: Test
```

## TRIGGER
---
*(Instrumental)*

--- from [Mystical Sun - "Overview Effect"](https://www.youtube.com/watch?v=FIe7M5QMWUg&list=OLAK5uy_l8JhRjIS2Ac6MzqK6WzlbN8xC43pngb3Y&index=2&t=0s)

## RESOURCES
---
[Perturbation Theory](https://en.wikipedia.org/wiki/Perturbation_theory)

## ECO
---
The Overview Effect argues that the world education systems are too complex, and too wasteful. Such complexity creates barriers that separate people into classes: violating the law of [Balance](/posts/theories/balance). The cost of college is a financial barrier. The cost of a poor childhood education is a mental barrier. The cost of too much education can be an unwillingness to explore new ideas. The combined damage from poor education is incalculable, and its effects permeate all of society.

We argue that there is power in simplicity and abstraction. At least initially, while teaching a new concept, detail is unimportant; what matters is context, putting the student into a certain state of mind, and assisting their development of positive beliefs. A student must master the foundation of an idea before they can move on to an advanced version of it.

Further, we should not be moving on to advanced ideas at all! Students need to know that something exists - and what it's used for - but in most cases, they do not need to know how to do it. 

For example, [Malcolm](/docs/personas/fodder) spent 13 years working in deeply-complex, deeply-technical information systems. Never once did he need to use mathematics beyond a basic level of algebra.

Stop wasting everyone's time with learning things that they will soon forget! Teach them WHAT exists, and WHY it exists - and allow them to learn HOW of their own accord, should the need arise!

This theory uses technology to provide a remedy to the broken, wasteful education systems of today. It uses self-discovery and personal engagement to foster excitement about learning. In this paradigm, Humanity has come to accept that all experience is [subjective](https://lmgtfy.com/?q=subjective+definition). As such, we treat all ideas with respect, dignity, and positivity - so long as they do the same in return. We attempt to [Integrate](/posts/theories/integration) beliefs into our own where possible. 

Most importantly, we find ways to relate to other people's version of truth. How do we do this? The answer is simple:

The average human is susceptible to story. If we were able to deliver education more effectively, we could rehabilitate the world. What would such a tool require?

1. We need a story to tell. 
2. We need a universal language; a "proxy code" that sits in-between all languages (truly, it must sit BEFORE all other languages).
3. We need a medium to deliver the story (i.e. [The Fold](/posts/theories/fold)).

In achieving these goals, we will have built a tool able to facilitate all types of learning - and able to tell any type of story. Education will become the model for law, technology, science, psychology, politics, economics, and intelligence. Our hope is that this will allow others to influence society in a more-direct way. We rule from the middle class, rather than from the extremes.

Through the [The Great Filter](https://www.amazon.com/Great-Filter-TUB-RING/dp/B000OCY69K) of our mind, we will find our own, personal truth. 

In doing so, we expect to find an unexpected universal truth:

## PREDICTION
---
```
Time travel is real.
```