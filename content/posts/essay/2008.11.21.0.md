---
author: "Luciferian Ink"
date: 2008-11-21
title: "How Antivirus Works"
weight: 10
categories: "essay"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A college assignment.

## ECO
---

Whether it was the first computer viruses created as experiments in the eighties or the advanced malware capable of hijacking a user’s PC today, one of the largest threats to computer security has always been these malicious programs.

In the past, there was but one way to prevent against such things. Effectively placing the computer in a bubble, one could not connect to a network, use floppy disks or CD’s, nor go online. While this method is “effective”, it renders the machine virtually useless for anything but the simplest of tasks.  One may have access to blistering data processing, but have no data to process. This method, obviously, was unacceptable.

There is another, less secure, but more tolerable method. In 1987, the first known virus, Vienna, was neutralized by a man named Bernt Fix, but after the relative success of this first known threat, others followed suit. Soon after was the introduction of the first programs designated to prevent against these threats; thus antivirus was born.
What, exactly, is antivirus preventing though? In general, there are several basic types of program that antivirus looks for.

The first is spyware and adware. Spyware is installed secretly onto a user’s computer with the intent to take partial control or intercept information without the user’s consent. Adware are programs posing as legitimate links, usually with intent of spreading more spyware, adware, and viruses. Spyware monitors user behavior, and attempts to collect personal information. Spyware programs can collect multiple types of personal information, like sites that have been visited, the habits of your internet surfing. Not simply that; they may do things such as installing additional software, redirecting your browser, connecting to dangerous websites and downloading viruses, or diverting advertising money to a third party. These types of programs are what will lead to identity theft and credit card fraud.

A second type is the ever-present “virus”. A virus is a very general term for any program that can make copies of itself and infects a user’s computer without any input. Malware, meaning malicious software, is simply another term for a virus. These programs are a set of instructions written so that a computer can process them. A virus can only spread from one computer to another when its host is taken to the uninfected computer. Since most users are connected to the internet or a local area network at all times, the spread of malicious code is at an all time high. Some viruses are made to damage the PC by attacking programs, deleting files, or reformatting the hard drive. Other malware programs may not be designed to hurt anything at all, but simply replicate themselves and make their presence known by presenting visual or audio proof of that. If nothing else, this malware uses valuable system resources, often are bug ridden, and the sheer volume they are capable of replicating at can bring many older PC’s to the point of crashing beyond repair. Perhaps because of the broadness of the term “virus”, people often confuse any and every computer problem with a virus.

Other types of programs are actually two; similar to viruses but fundamentally different. The worm is a program that spreads through a network without having to be part of a host, while a Trojan horse is a program that poses as another legitimate program. These two are categorized separately because of how difficult it is to find and destroy them. Because they are always changing and hiding, often not visible when not running, they can be near impossible to get rid of.

Another type is macro viruses, which are arguably the most destructive and widespread computer viruses to be found. A macro is a series of actions that help to automate specific tasks, such as a command to run a program on a computer. These can be so dangerous because, unlike other viruses, they require little to no user input. Everyday programs such as Microsoft Office support macros in many of their file types, and all it takes is to open that .doc in your inbox to become infected.

On the outside, antivirus is nothing more than a system of scanning information to find these infections, and deleting the known threats. Inside, it is much more than this.

Many antivirus programs scan on a source to destination basis. The information being scanned, or the “source”, is treated differently depending on the media it originates from. The source can be anything from a USB thumb drive to a server in which a message is stored, while the destination may be your hard drive or the communication system in your machine, respectively. In the case of the former, the thumb drive may be scanned upon transfer of files to the hard drive, with each read and write call being intercepted and inspected for known infections and/or suspicious actions (such as executable file extensions or text macros). All of this is done with an interpretation mechanism that is specific and unique to each operating system. If an infection is found, the program and those similar to it are detained/quarantined, even before alerting the user. This, however, is not the only way.

Other antivirus programs do not run under an operating system. For example, a firewall may provide the antivirus with the information it needs to scan each protocol and port that is used, such as email and the internet. The interpretation mechanism is essentially built right in.

Still others do not have an interpretation mechanism at all. Here, special mechanisms between the antivirus and the application must be used. The process is summed up well by the International Technical Editor of Panda Software.

1. The cleaned information is returned to the interpretation mechanism, which in turn will return it to the system so that it can continue towards its final destination. This means that if an e-mail message was being received, the message will be let through to the mailbox, or if a file way being copied, the copy process will be allowed to finish.
2. A warning is sent to the user interface. This user interface can vary greatly. In an antivirus for workstations, a message can be displayed on screen, but in server solutions the alert could be sent as an e-mail message, an internal network message, an entry in an activity report or as some kind of message to the antivirus management tool.

How do these scans detect these viruses, if they are changing and adapting all the time? Virus scan engines work in two ways. One method compares known viruses, call virus signatures, with similar files on the infected system, in an attempt to quarantine and remove them. The other way is called heuristic scanning, and compares a virus’ behavior pattern with those of known viruses, attempting to stop these malicious acts from happening.

Before getting into that, it is important to know how these programs actually scan. One way is called permanent scanning constantly monitors a computer’s actions to stop intrusive behaviors before they begin. On demand scans are more robust, used for special circumstances in which a user may want to scan specific areas of a computer, such as a flash drive, for viruses. Many free antivirus and anti malware only operates in this fashion, in which case it is very important to do often.

Used together, both should be able to locate and scan each potential unwanted program on a computer. Whether or not they actually find a threat is another problem entirely. Usually, utilizing virus signatures and heuristic scanning is the best way to ensure positive results. Individually, these methods have problems. 

The signatures method must be updated often, as new viruses are discovered every day. Also, a virus must be known for an antivirus program like this to find it at all. Still, certain malware programs might have morphing functions that rewrite the instructions each time they are executed. As a result, the known pattern that might have matched before the executing the program may not match after being executed. Even beyond the virus part, there are almost 400,000 known viruses out there. To be able to scan for every virus with each pass is not only unheard of, but nearly impossible without bringing even the most advanced PC to its knees.

Heuristic scanning will often alert the user to problems that do not exist; for example, attempting to format a hard drive or run an executable file may lead to an alert (Windows Vista anyone?). The problem with this is that the author of the malware can simply create his program specifically for bypassing the list of what heuristic scanners look for.

There is actually a third way. This way is the most reliable, but also the most difficult to prepare. This process is called white listing, and operates as follows. Every file on a system has a unique fingerprint, and as a result, it is fairly easy to create a database of a whole file system, and compare each and every program that tries to run with the database. If the process is a known fingerprint, it is allowed to run; otherwise, the user is alerted and allowed to run the program if deemed safe.

Another form of virus prevention is the firewall. A firewall is an integrated – either physically or through software – collection of security procedures used to permit, deny, encrypt, decrypt, or proxy any and all computer traffic through it, so as to prevent unauthorized access to a networked system. A firewall's basic use is to regulate the flow of traffic between computer networks of different trust levels. The internet is a zone with no trust and an internal network is a zone of higher trust, to name a few examples. While a firewall can and is highly effective, it is pointless to the many users who do not configure it correctly. Often a firewall is installed with a "default-deny" firewall rule set, in which the only network connections permitted are the ones that have been explicitly programmed to be let through the firewall. Unfortunately, such a configuration requires detailed understanding of the network, which many businesses lack. Therefore, many of these companies will implement a "default-allow" rule set, in which, unless it has been specifically blocked, all traffic is allowed. This set-up makes unintended network connections and file system compromise more plausible.

Perhaps the best virus prevention of all is user education. If only every user knew that one should be careful when opening an email from an unknown sender, or even a friend, should a file be attached. Never run a program if you were not the one to initiate it, or if you are unsure about the legitimacy of the author. Always keep your antivirus, your operating system, your programs, up to date. Be careful what links you click – never those pop-up ads – when visiting any website. Do not download hacked or illegal programs and files, that is simply asking for trouble.

Unfortunately, no one can be perfect, and even the most careful of people are at risk. This is why one must take preventative measures against this spyware, malware, and adware that we are plagued with everyday. This is why we must use every form of antivirus we can find, lest we want to lose our identity, get our credit card stolen, or be the sender of thousands of spam emails. Use that antivirus that constantly scans your PC, use that antimalware program to make sure you don’t have the beginnings of a malicious program on your computer, use that antispyware to make sure no one is watching you. Always make sure you have a firewall on; both hardware and software, if you wish to be really safe. And always practice safe internet use. In this way, we can get as close to completely protected as possible.

### Works Cited
Dupree, Janet R. "Avoid online viruses by vaccinating your computer." U.S. News & World Report 29 May 2000.

Fogie, Seth. "How Antivirus Programs (Don’t) Work." InformIT. 28 Dec. 2007. 21 Nov. 2008 <http://www.informit.com/guides/content.aspx?g=security&seqnum=288>.

Gregory, Peter H. Computer Viruses for Dummies. New York: For Dummies, 2004. 

HighBeam Encyclopedia. 2008. HighBeam Research, Inc. 21 Nov. 2008 <http://www.encyclopedia.com>.

Ingham, Kenneth, and Stephanie Forrest. A History and Survey of Network Firewalls. Cs.unm.edu. Vers. 5. University of New Mexico. 21 Nov. 2008 <http://www.cs.unm.edu/~treport/tr/02-12/firewall.pdf>.

La Cuadra, Fernando D. "How an Antivirus Program Works." Help Net Security. 7 May 2003. Panda Software. 21 Nov. 2008 <http://www.net-security.org/article.php?id=485&p=3>.