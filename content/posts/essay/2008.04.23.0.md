---
author: "Luciferian Ink"
date: 2008-04-23
title: "Should Evolution Be Taught in Schools?"
weight: 10
categories: "essay"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A college assignment.

## ECO
---

As it stands, evolution theories (Creationism, Darwinism, etc.) are widely taught scientific beliefs that are becoming a raging battle between many non-believers and the scientific community. Nearly every school in the country touches on the subjects, and Darwinism is currently the most widely taught theory in the nation (Bergman 2). There are places, though, where these theories are contested on the basis of their validity. Schools aren’t pretending these theories are fact. They are viable theories that all people, even the groups who oppose them, should be informed of.

While probably not entirely true, there is obvious proof that the Darwinistic evolution theory, to a degree, does exist. On Charles Darwin’s trip around the world in 1831, he was astonished at how identical species could be perfectly adapted to the environment in which it inhabited. The iguanas of the Galapagos Islands, for example, had adapted a color "as black as the porous lava rocks over which they crawl,” while the creatures inland were a more common green color (“A Trip” 1). Certainly, no one of any belief can refute this proof.

Nor can anyone refute the fact that there are current forms or evolution that we can prove. Just look at the difference between the malnourished in a third world country, and the obese in America. In both cases, the human body has had to adapt to its environment, and proves to be resilient.

While this seems to make sense, there is little scientific proof that man evolved from a different species entirely. That doesn’t rule out the fact that it is still possible. This should still be taught in schools across the nation as an applicable theory. Darwinism, strangely, does not hold as much opposition as one might think. Creationism is a much more widely contested theory, and for many reasons. 

The foremost reason is challenged on the grounds of religion. After all, Creationism holds that God created the world about 6,000 years ago. Thus the theory was banned in 1987 by the Supreme Court took keep with the separation of church and state as given in the Constitution. This came as a major victory to parents and teachers in opposition to the matter, but did not deter creationists. Intelligent Design was formed, changing their position to "…a scientific disagreement with the claim of evolutionary theory that natural phenomena are not designed.'' (Badkhen 17) A specific God is never mentioned in this theory.

Herein lies the problem. Individuals across the country continue to fight school boards on this because they believe that Intelligent Design is a Trojan horse for religion in the classroom. 11 parents In Dover, Pennsylvania were outraged when a school board vote required teachers to teach “alternatives” to evolution. The American Civil Liberties Union (ACLU) backed these parents, seeking to put Intelligent Design on trial, and hoped to discredit it entirely, so that no other school would dare teach the subject. To Witold J. Walczak, legal director of the ACLU of Pennsylvania, intelligent design is “…not science because it does not meet the ground rules of science, is not based on natural explanations, [and] is not testable." (Goodstein 1-15) Gary Demar of the American Vision group refutes that scientific method cannot be applied to evolution theories because, "It's never been observed. You can't repeat the experiment. And so what's being sold as science, in terms of evolution, really isn't science…” (Cabell 7)
	
This type of opposition is not exclusive to one school either. The Kansas Board of Education ruled in 1999 to have Darwin’s theory removed from state curriculum, and left to the teachers to decide whether or not to teach it. Not only has this angered the science community, it has leaves the children at the mercy of their institution; they may learn one thing one year, and another the next. “This act… took us back 100 years in science teaching and education," says Barry Lynn, executive director of Americans United for Separation of Church and State. (Cabell 1-2)

While lack of evidence may leave some people uncertain about both theories, they are still valid, and I see no reason why they should not be taught in public schools, alongside each other and any other major theories. Certainly a firm believer in their faith will not be swayed by simply hearing a theory; evolution is not being impressed on them. After all, a theory is no more than a plausible belief, and since there is no conclusive proof for either theory, they should be looked at with open eyes.

This is what I wish to propose. On the basis that these theories are just as they sound - theories - I believe that they should continue being taught as such. All major theories should be included. All people should have the opportunity to learn of these while in school, but none should be required to listen if they so choose. Give them an alternative; perhaps have them write a paper on how they believe they were created. Do not make for a long and drawn out discussion; simply go over the basics of each theory and move on to the next. Do not test them on it. In this way, students will be informed, and able to make a decision on their own as to what they believe.

I strongly believe that believe that evolution theory, and all major creation theories, should continue to be taught in public schools. It is not being taught as truth, merely a viable idea that even the most devout in their beliefs should know about, if only to know what they don’t believe in.

### Works Cited
“A Trip Around the World” 2008. American Museum of Natural History. 13 Feb. 2008: <http://www.amnh.org>	

Badkhen, Anna. “Anti-evolution teachings gain foothold in U.S. schools”. San Francisco Chronicle. 30 Nov. 2004: 37 pars. 21 Apr. 2008. <http://www.sfgate.com>

Bergman, Jerry. “Teaching Creation and Evolution in Schools”. Nov. 1999. Answersingenesis.org. 20 Feb. 2008. 26 par. <http://www.answersingenesis.org>

Cabell, Brian. “Kansas School Board's Evolution Ruling Angers Science Community”. CNN. 12 Aug. 1999: 8 par.<http://www.cnn.com> 

Goodstein, Laurie. “A Web of Faith, Law and Science in Evolution Suit”. New York Times. 26 Sep. 2005: 30 pars. 17 Apr. 2008 <http://www.nytimes.com>