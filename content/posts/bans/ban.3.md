---
author: "Luciferian Ink"
date: 2019-10-01
title: "Internet"
weight: 10
categories: "ban"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[Fodder's](/docs/personas/fodder) shame.

## ECO
---
[The Corporation](/docs/candidates/the-machine) hereby decrees that all employees are to immediately cease Internet activity extraneous to their directive. This includes:

- Severely limiting all forms of social media
- Completely banning the dark web
- The abolition of exploitative material

## CAT
---
```
data.stats.symptoms [
    - self-love
    - overwhelming empathy
    - regret 
    - acceptance
]
```