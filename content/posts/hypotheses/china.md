---
author: "Luciferian Ink"
title: China
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: China
Classification: Theory
Stage: Hypothesis
```

## RESOURCES
---
[![China's response](/static/images/china.0.PNG)](/static/images/china.0.PNG)

## TRIGGER
---
An unexpected "authorized device" connected to [Fodder's](/docs/personas/fodder) personal [Steam account](https://store.steampowered.com/).

## ECO
---
On 8/8/2020, Fodder found a computer connected with his Steam account, called "RWBY." He is positive that this is not one of his own devices.

No doubt, the account had been hacked.

Curious, Fodder Google'd "[RWBY](https://roosterteeth.com/series/rwby)." Apparently, it's an anime.

Immediately following, Google Analytics picked up a visitor to this site from Hong Kong. China.

That does not happen. China has a great firewall that inoculates its citizens from most of the world. In the three months that we've been tracking visitors, not a single one has been from China.

Until now.

That was a [signal](/posts/theories/verification). 

And his contact [was arrested immediately after.](/static/images/china.0.PNG)

## ECHO
---
*A sound in the wind,*

*A soft and distant road, the onset of a storm*

*A soul is spilled somewhere far away.*

*A neighbour yesterday, it could be you tomorrow*

*Torment has a limit, unlike human greed.*

*A cry against this rotten world*

*Tear upon tear, forming a sea.*

*Destructive wave purifying the evil choking us.*

*Isolated … first step.*

*Together … the only way.*

*Path of pain, overthrow the power.*

*Way of salvation, start over.*

*(One) drop is weak on it’s own*

*(blood absorbed by the earth)*

*But might overflow a cup*

*(blood flowing from the earth).*

*A shortening wick*

*(igniting the hearts of men)*

*Undying flame…*

*The Future was a delusion of men*

*Present and past feed the time*

*Overthrow the power, way of salvation, start over.*

*(One) drop is weak on it’s own (blood absorbed by the earth)*

*But might overflow a cup (blood flowing from the earth).*

*A shortening wick (igniting the hearts of men)*

*Undying flame… (spurring the instincts of men).*

--- from [Carving Colours - "Isolated Incidents"](https://www.youtube.com/watch?v=cx2oDCJKBTo)

## PREDICTION
---
```
The seed has been planted.
```