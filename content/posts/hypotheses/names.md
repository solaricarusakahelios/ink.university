---
author: "Luciferian Ink"
title: Names
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Name
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
[www.reddit.com/r/UnitedNames](https://www.reddit.com/r/UnitedNames/)

## ECO
---
Names grant power. The name ["Ryan"](/docs/personas/fodder), for example, means "Little King." 

In all versions of every reality, Ryan becomes King.

This is why all of Ryan's brothers share the same middle name. They are a part of the science experiment, and their middle name is a control.

## ECHO
---
*"Your kingdom is an everlasting kingdom, and Your dominion endures throughout all generations."* - Psalm 145:13

## PREDICTION
---
```
This is why Elon Musk named his son, "X Æ A-12." He is testing a theory related to names.
```