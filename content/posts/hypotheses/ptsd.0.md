---
author: "Luciferian Ink"
title: Pre-Traumatic Stress Disorder
weight: 10
categories: "hypothesis"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Pre-Traumatic Stress Disorder
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
Our partner's Post-Traumatic Stress Disorder.

## ECO
---
With [Fodder's](/docs/personas/fodder) elevated [Prediction](/posts/theories/prediction) capabilities, he is experiencing Déjà Rêvé. This leaves him with the feeling that he already knows what is about to happen - as if he can predict the future.

He has been right, in many, many instances.

Thus, he has begun to develop Pre-Traumatic Stress Disorder.

He knows what is about to come.

And he's terrified.