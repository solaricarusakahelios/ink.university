Author date: 2012-05-07

Characters
1. Protagonist
   1. Appearance
      1. Clothing
         1. Wears a tattered greenish-brown trenchcoat
         2. Carries a pistol or revolver, is an excellent shot
         3. Possibly carries a sabre, sword, etc
         4. Wears a cowboy hat, or some other kind of hat
         5. Carries a pack
         6. Rides a bike of some sort
      2. Looks
         1. Constantly changing his looks
            1. Espionage makes this a necessity
         2. Disheveled facial hair
         3. Constant serious look, never smiles
         4. Possibly sunglasses or a scar
   2. Personality
      1. Cold
         1. Unsympathetic toward problems that he does not deem important
         2. Often emits an air of cynicism to mask his true feelings
      2. Cares about the greater good
         1. Looks at the “big picture,” and is actively looking for ways to help others
            1. He is not above breaking the law/morals to get a job done
      3. Sarcastic
         1. Often comes off as snide to others
         2. Comments would generally be funny to the reader
         3. “Easter Eggs,” things that only the reader might catch
   3. Works for a large corporation
      1. “Cleans up” corporate mistakes
         1. Failed raids – cleans up bodies, mess
         2. Disables/threatens those that know too much
         3. Espionage
      2. He hates this job
         1. Though he sees merit in staying
            1. Needs the money to fund his addiction
            2. Suspects that there is more to them than the public can see
            3. Believes in the end, even if he can't justify the means
      3. He will eventually need to befriend a higher-up to gain access to confidential information
         1. This higher-up will likely be female
            1. He will use her, but he will never feel for her
            2. She will feel for him, and kill herself when she finds out that he has been lying
               1. This will be the first time he relapses in the book
                  1. Will be crack, heroin, or some other kind of “hard” drug
   4. Is a recovering addict
      1. Has been sober for years
         1. Quit his “hard” drugs long ago.
         2. Doesn't drink, but still smokes
      2. Will relapse when his girlfriend commits suicide
   5. Backstory
      1. Came from a broken home
         1. Father was an abusive alcoholic
         2. Mother was abused
            1. Died when he was young
               1. Cause of death was inconclusive, though implied that the father killed her
      2. Was kicked out of multiple schools
         1. Reasons
            1. Fighting
            2. Drugs
      3. Enlisted at 17
         1. Served time fighting for the United Front
            1. Deployed in Africa
               1. Fought powerful warlords
               2. Gov't was attempting to convert other countries to the United Front's cause
            2. Recalled when the gov't ran out of money
            3. Was given medals for some of his exploits
         2. Later served in government espionage
            1. Had been known to even spy on “states” within the United Front
            2. Was dishonorably discharged when he came too close to discovering some backdoor deals with major corporations
               1. This is what pushed him to drugs in the first place
                  1. He is in huge amounts of debt because of it
         3. Finally landed a corporate job
            1. Works for X Corporation
               1. Specialized in:
                  1. Espionage
                  2. Intelligence
                  3. Coverups
               2. Does not agree with their methods
                  1. Continues because he cannot afford not to
      4. Story
         1. Finds a suspicious item while on a mission
            1. Takes it to Old Guy X
               1. Begins to discover a corporate consipiracy within the government
            2. Must inflitrate corporation and government
   6. Has some sort of special ability
      1. A result of irradiation
      2. Causes a “tweak”
         1. Incapasitates him at times
         2. Like a shock down his spine
   7. Old Guy X
      1. Great at salvaging old technology for other uses
   8. Contact/Love Interest
      1. Works for Corporation Z
      2. Will assist Protagonist in researching/developing salvaged technology
   9. The Homeless
      1. Other names
         1. The degenerates, the nameless, the faceless, the delinquents
      2. Zero
         1. Early contact
         2. War buddy
         3. Roger Lambert
2.